import { storage } from './firebase';

export const uploadImage = (file, callback) => {
  const ref = storage.ref();
  const name = (+new Date()) + '-' + file.name;
  const metadata = {
    contentType: file.type
  };
  const task = ref.child(name).put(file, metadata);
  task
    .then(snapshot => snapshot.ref.getDownloadURL())
    .then((url) => {
      callback(url);
    })
    .catch(console.error);
};

