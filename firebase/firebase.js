import firebase from "firebase/app";
import "firebase/storage"

const config = {
  apiKey: 'AIzaSyDE0x1FrC6lQzq7mnq3G91VuAiODIEtl_o',
  authDomain: 'cse-wallet.firebaseapp.com',
  databaseURL: 'https://cse-wallet.firebaseio.com',
  projectId: 'cse-wallet',
  storageBucket: 'cse-wallet.appspot.com',
  messagingSenderId: '952003705837'
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const storage = firebase.storage();

export { storage };
