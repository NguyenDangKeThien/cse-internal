import * as storage from './storage';
import * as firebase from './firebase';

export {
  storage,
  firebase,
};