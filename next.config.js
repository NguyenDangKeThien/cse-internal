require('dotenv').config();
const webpack = require('webpack');
const _env = require('dotenv-webpack');
const path = require('path');

module.exports = {  
    pageExtensions: ['mdx', 'jsx', 'js', 'ts', 'tsx'],

    webpack(config, options) {
        config.plugins = config.plugins || [];
        config.plugins = [
            ...config.plugins,
            // Read the .env file
            new _env({
                path: path.join(__dirname, '.env'),
                systemvars: true
            })
        ];
        
        return config
    }
}