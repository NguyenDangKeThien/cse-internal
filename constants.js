const API_PREFIX = process.env.HOST ? process.env.HOST + ":" + process.env.API_PORT + "/" : "https://csewallet.io:6969/";

export const CSE_GET_WALLET_API = API_PREFIX + 'v1/wallet';
export const CSE_GET_TRANSACTION_HISTORY_API = API_PREFIX + 'v1/wallet/CSE/histories';
