import React, { Component } from 'react';
import Head from 'next/head';
import Router from 'next/router';
import AuthService from '../services/auth.service';
import http from '../plugins/http';

// Import components
import Sidebar from '../components/client/Sidebar';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { setUserData } from '../redux/actions';
import { fetchWalletData } from '../redux/actions/wallet-actions';
import WindowSizeListener from 'react-window-size-listener';

class Client extends Component {

    initalState = {
        user: this.props.user,
        isMin: false,
        showResponsiveMenu : false
    }

    constructor(props) {
        super(props);

        this.state = this.initalState;
        if(AuthService.isAuth) {
            this.setUser();
            this.props.fetchWalletData();
        }
    }

    onResize = (windowSize) => {
        if (windowSize.windowWidth > 768) {
            this.setState({showResponsiveMenu: false});
        }
    }

    componentDidMount() {
        this.checkAuthentication('init');
    }

    componentDidUpdate(prevProps) {
        if(prevProps.user !== this.props.user) {
            this.setState({
                user: this.props.user,
            })
        }
    }

    checkAuthentication = (action) => {
        const { router } = this.props;
        const pathCheck = [
            '/login',
            '/signup',
            '/forgot',
            '/forgot-confirm-code',
            '/new-password',
            '/verify-email',
        ]
        if (pathCheck.indexOf(router.pathname) === -1 && !AuthService.isAuth) {
            if (action == 'init') {
                Router.push({ pathname: '/login' });
            }
            if (action == 'render') {
                return false;
            }
        }else if(pathCheck.indexOf(router.pathname) >= 0 && AuthService.isAuth) {
            if (action == 'init') {
                Router.push({ pathname: '/' });
            }
            if (action == 'render') {
                return true;
            }
        }else {
            return true;
        }
    }

    setUser = () => {
        http.get('auth/me')
        .then(response => {
            const user = response.data.result.user;
            this.props.setUserData(user);
        })
    }

    minSidebarFunction = () => {
        this.setState({
            isMin: !this.state.isMin
        })
    }



    render() {
        const { children, router } = this.props;
        const pathCheck = [
            '/login',
            '/signup',
            '/forgot',
            '/forgot-confirm-code',
            '/new-password',
            '/verify-email',
        ]
        return (
            <div style={{height: 'auto !important', width: 'auto !important'}}>
                <WindowSizeListener onResize={this.onResize}/>
                <Head>
                    <title>CSE Wallet</title>
                    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
                    <link rel="stylesheet" href="https://unpkg.com/swiper/css/swiper.min.css"></link>
                    <link href="/client-asset/css/main.css" rel="stylesheet" />
                    <link href="/library-asset/css/toastr.min.css" rel="stylesheet" />
                </Head>
                {
                    pathCheck.indexOf(router.pathname) === -1 && this.checkAuthentication('render')
                    ?
                    <div className="d-block bgr-dash h-fit">
                        <div className="position-relative p-0">
                            <div className={`d-block d-lg-flex position-fixed h-0 zIndex10 translate-3 ${ this.state.isMin ? 'col-1' : 'col-2' }`}>
                                <button className="d-lg-block d-none btn sidebar-min-btn" type="button" onClick={this.minSidebarFunction}>
                                    <svg width="8" height="11" viewBox="0 0 8 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        {
                                            this.state.isMin
                                            ?
                                            <path d="M7.68032 5.26006C7.84021 5.38007 7.84021 5.61993 7.68032 5.73994L0.980085 10.7688C0.782328 10.9173 0.5 10.7762 0.5 10.5289L0.500001 0.471101C0.500001 0.223838 0.782329 0.0827372 0.980086 0.231165L7.68032 5.26006Z" fill="white"/>
                                            :
                                            <path d="M0.319677 5.73994C0.159791 5.61993 0.159791 5.38007 0.319678 5.26006L7.01991 0.231165C7.21767 0.0827367 7.5 0.223838 7.5 0.4711L7.5 10.5289C7.5 10.7762 7.21767 10.9173 7.01991 10.7688L0.319677 5.73994Z" fill="white"/>
                                        }
                                    </svg>
                                </button>
                            </div>
                            <Sidebar isShow={this.state.showResponsiveMenu} setIsShow={ (value) => {this.setState( { showResponsiveMenu: value })}} user={ this.state.user } router={ router } isMin={ this.state.isMin } />
                        </div>
                        <div onClick={ () => this.setState( { showResponsiveMenu: false })} className={`px-30 py-md-4 py-2 text-white ml-lg-auto translate-3 bgr-dash pt-header min-h-100vh ${this.state.isMin ? 'col-lg-11' : 'col-lg-10'}`} style={{opacity : this.state.showResponsiveMenu ? 0.3 : 1 }}>
                            { this.checkAuthentication('render') ? children : '' }
                        </div>
                    </div>
                    :
                    <div className="d-block bgr-dash h-fit">
                        { this.checkAuthentication('render') ? children : '' }
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user,
    wallet: state.wallet,
});
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    { setUserData, fetchWalletData },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Client);