import axios from 'axios';
import AuthService from '../services/auth.service';

const apiUrl = `${ process.env.HOST }:${ process.env.API_PORT }/v1/`;

/**
 * Create Axios
 */
const http = axios.create({
    baseURL: apiUrl,
})


/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our NodeJS back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
http.interceptors.request.use(
    async (config) => {
        const token = AuthService.accessToken || AuthService.responseToken
        config.headers.common['access-token'] = token;
        return config;
    },
    error => {
        return Promise.reject(error);
    },
);

/**
 * Handle all error messages.
 */
http.interceptors.response.use(function (response) {
    return response;
}, function (error, res) {
    const { response } = error;
    if (response && response.status >= 301 && response.status <= 451) {
        if (response.status == 401) {
            
        }
        if(response.status == 413){
            
        }
        return Promise.reject(response);
    }

    return Promise.reject(response);
});

export default http;