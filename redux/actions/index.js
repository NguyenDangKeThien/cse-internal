import { USER_DATA, SET_CODE_INPUT_FORM_ERROR, RESET_CODE_INPUT_FORM_ERROR } from './type.js';

// Action add user_data
export const setUserData = (data) => {
    return {
        type: USER_DATA,
        data,
    }
}

export const actSetCodeInputFormError = (data) => {
    return {
        type: SET_CODE_INPUT_FORM_ERROR,
        data,
    }
}

export const actResetCodeInputFormError = () => {
    return {
        type: RESET_CODE_INPUT_FORM_ERROR
    }
}
