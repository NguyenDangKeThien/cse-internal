import axios from "axios";
import {
	SET_HISTORY_DATA,
	SET_HISTORY_WALLET_TYPE,
	SET_HISTORY_TRANSFER_TYPE,
	SET_HISTORY_CURRENT_PAGE,
	SET_WALLETS_PAGING_RESULT,
    SET_HISTORY_FETCHING,
    SET_HISTORY_CURRENT_PAGE_LIMIT,
} from '../actions/type.js';
import AuthService from '../../services/auth.service';
import {CSE_GET_TRANSACTION_HISTORY_API} from '../../constants';

export const setHistoryData = (data) => dispatch => {
    return dispatch({
        type: SET_HISTORY_DATA,
        payload: data,
    });
}

export const setHistoryWalletType = (data) => async dispatch => {
    await dispatch({
        type: SET_HISTORY_WALLET_TYPE,
        payload: data,
    });
    return dispatch(fetchHistoryData());
}

export const setHistoryTransferType = (data) => async dispatch => {
    await dispatch({
        type: SET_HISTORY_TRANSFER_TYPE,
        payload: data,
    });
    return dispatch(fetchHistoryData());
}

export const setHistoryCurrentPage = (data) => async dispatch => {
    await dispatch({
        type: SET_HISTORY_CURRENT_PAGE,
        payload: data,
    });
    return dispatch(fetchHistoryData());
}

export const setHistoryCurrentPageLimit = (data) => async dispatch => {
    await dispatch({
        type: SET_HISTORY_CURRENT_PAGE,
        payload: 1,
    });
    await dispatch({
        type: SET_HISTORY_CURRENT_PAGE_LIMIT,
        payload: data,
    });
    return dispatch(fetchHistoryData());
}

export const setHistoryPagingResult = (data) => async dispatch => {
    return dispatch({
        type: SET_WALLETS_PAGING_RESULT,
        payload: data,
    });
}

export const fetchHistoryData = (address = 'CSE5AFFF987E8AE9A6EA05AAAED') => async (dispatch, getState) => {
    await dispatch({
        type: SET_HISTORY_FETCHING,
        payload: true,
    });
	const historyReducer = getState().historyReducer;
	let params = { page : historyReducer.page, limit: historyReducer.limit, type: historyReducer.transferFilterType, address: address };
	axios.get(CSE_GET_TRANSACTION_HISTORY_API, { 'headers': { 'access-token': AuthService.accessToken },  params: params })
	.then((async response => {
		if(response.data.success){
			let result = response.data.result;
			await dispatch(setHistoryData(result.docs));
			await dispatch(setHistoryPagingResult({
				total : result.total,
				page  : result.page,
                limit : result.limit,
				pages : result.pages,
			}))
            await dispatch({
                type: SET_HISTORY_FETCHING,
                payload: false,
            });
		}
	})).catch(function (error) {
        dispatch({
            type: SET_HISTORY_FETCHING,
            payload: false,
        });
	    console.error(error);
	});
}