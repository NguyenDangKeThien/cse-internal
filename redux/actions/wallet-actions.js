import axios from "axios";
import { SET_TOTAL_ESTIMATE_USTD, SET_WALLETS_DATA, SET_WALLETS_SELECT, SET_WALLET_REDUCER, SET_CHART_DATA, SET_WALLETS_QR_CODE } from './type.js';
import AuthService from '../../services/auth.service';
import {CSE_GET_WALLET_API} from '../../constants';
import * as moment from 'moment';

export const setWalletData = (data) => dispatch => {
    return dispatch({
        type: SET_WALLETS_DATA,
        payload: data,
    });
}

export const setTotalWstimateUstd = (data) => dispatch => {
    return dispatch({
        type: SET_TOTAL_ESTIMATE_USTD,
        payload: data,
    });
}

export const selectWallet = (wallet) => dispatch => {
    return dispatch({
        type: SET_WALLETS_SELECT,
        payload: wallet,
    });
}

export const fetchWalletData = () => async dispatch => {
	axios.get(CSE_GET_WALLET_API, { 'headers': { 'access-token': AuthService.accessToken } })
	.then((response => {
		if(response.data.success){
			let result = response.data.result
			dispatch({
				type: SET_WALLET_REDUCER,
			    payload: {totalEstimatedUSDT : result.totalEstimatedUSDT, wallets: result.docs},
			});
		}
	})).catch(function (error) {
	    console.error(error);
	});
}


const serialize = (obj) => {
	const str = [];
	for (const p in obj)
	  if (obj.hasOwnProperty(p)) {
		str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	  }
	return str.join("&");
}

const getStartTime = (type) => {
	let startTime = '';
	switch (type) {
		case 'hour': {
			startTime = moment().subtract(1, 'hours').unix();
			break;
		}
		case 'day': {
			startTime = moment().subtract(1, 'days').unix();
			break;
		}
		case 'week': {
			startTime = moment().subtract(1, 'weeks').unix();
			break;
		}
		case 'month': {
			startTime = moment().subtract(1, 'months').unix();
			break;
		}
	}
	return startTime;
}

const generateLabels = (type, data) => {
	const labels = [];
	switch (type) {
		case 'hour': {
			data.forEach((item, index) => {
				const label = moment.unix(item.timestamp).format('HH:mm');
				labels.push(label);
			})
			break;
		}
		case 'day': {
			data.forEach((item, index) => {
				const label = moment.unix(item.timestamp).format('HH:mm | DD.MMMM');
				labels.push(label);
			})
			break;
		}
		case 'week': {
			data.forEach((item, index) => {
				const label = moment.unix(item.timestamp).format('HH:mm | DD.MMMM');
				labels.push(label);
			})
			break;
		}
		case 'month': {
			data.forEach((item, index) => {
				const label = moment.unix(item.timestamp).format('DD.MMMM');
				labels.push(label);
			})
			break;
		}
	}
	return labels;
}

const generateDatasets = (type, data) => {
	const datasets = [];
	data.forEach((item, index) => {
		datasets.push(item.price);
	})
	return datasets;
}

export const fetchChartData = (type) => async dispatch => {
	const headers = { 'headers': { 'access-token': AuthService.accessToken } };
	const baseUrl = 'http://34.87.88.110:3000/api/v1/history';
	const params = {
		startTime: getStartTime(type),
		endTime: moment().unix(),
		basecoin: 'BTC',
		baseCoin: 'USD',
		quoteCoin: 'CSE',
		limit: 20
	}
	const paramsString = serialize(params);
	const url = `${baseUrl}?${paramsString}`

	axios.get(url, headers)
	.then((response) => {
		if (response.data.success) {
			const data = response.data && response.data.result;
			const labels = generateLabels(type, data);
			const datasets = generateDatasets(type, data);
			dispatch({
				type: SET_CHART_DATA,
				data: {
					labels,
					datasets
				}
			});
		}
	})
	.catch((error) => {
		console.log(error)
	})
}

export const getQRCodeFromString = (string) => async dispatch => {
	const baseUrl = 'https://api.qrserver.com/v1/create-qr-code/';
	const params = {
		size :"290x290",
		data : string
	}
	const paramsString = serialize(params);
	const url = `${baseUrl}?${paramsString}`
	axios.get(url)
	.then((response) => {
		return dispatch({
	        type: SET_WALLETS_QR_CODE,
	        payload: response.data,
	    });
		console.log(response);
	})
	.catch((error) => {
		console.log(error)
	})
}