import { SET_TOTAL_ESTIMATE_USTD, SET_WALLETS_DATA, SET_WALLETS_SELECT, SET_WALLET_REDUCER, SET_WALLETS_QR_CODE } from '../actions/type.js';

const initialState = {
	totalEstimatedUSDT : 0,
	wallets : [],
	walletSelected : 'CSE',
	cseAddressQrCode : "./images-asset/qrcode.png",
}; 

const walletReducer = (state = initialState, action) => {
    switch(action.type) {
        case SET_TOTAL_ESTIMATE_USTD:
            return{
				...state,
				totalEstimatedUSDT: action.payload
			};

        case SET_WALLETS_DATA:
            return{
				...state,
				wallets: JSON.parse(JSON.stringify(action.payload))
			};

		case SET_WALLETS_SELECT:
            return{
				...state,
				walletSelected: action.payload ? action.payload : initialState.walletSelected
			};
		case SET_WALLET_REDUCER:
            return{
				...state,
				...action.payload
			};
        default: return state;
    }
}

export default walletReducer;