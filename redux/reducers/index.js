import { combineReducers } from 'redux';
import userReducer from './user';
import formValidateReducer from './form-validate';
import walletReducer from './wallet';
import historyReducer from './history';
import chartReducer from './chart';

export default combineReducers({
    user: userReducer,
    formValidate: formValidateReducer,
    walletReducer: walletReducer,
    historyReducer : historyReducer,
    chart : chartReducer,
});
