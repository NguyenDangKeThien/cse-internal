import { SET_CODE_INPUT_FORM_ERROR, RESET_CODE_INPUT_FORM_ERROR } from '../actions/type.js';

const initialState = {
    formInputCode: {
        formErrors: {code: ''}
    }
};

const formValidateReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CODE_INPUT_FORM_ERROR: {
            return {
                ...state,
                formInputCode: {
                    formErrors: action.data.formErrors
                }
            };
        }
        case RESET_CODE_INPUT_FORM_ERROR: {
            return {
                ...state,
                formInputCode: {
                    formErrors: {code: ''}
                }
            };
        }
        default: return {...state};
    }
};

export default formValidateReducer;