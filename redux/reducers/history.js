import { SET_HISTORY_DATA, SET_HISTORY_WALLET_TYPE, 
	SET_WALLETS_PAGING_RESULT, SET_HISTORY_TRANSFER_TYPE, 
	SET_HISTORY_CURRENT_PAGE, SET_HISTORY_FETCHING, 
	SET_HISTORY_CURRENT_PAGE_LIMIT } from '../actions/type.js';

const initialState = {
	isFetching : false,
	histories 	 : [],
	walletFilterType   : 'CSE',
	transferFilterType : '',
	total 		 : 0,
	limit 		 : 10,
	page 		 : 1,
	pages 		 : 1,
}; 

const historyReducer = (state = initialState, action) => {
    switch(action.type) {
        case SET_HISTORY_DATA:
            return{
				...state,
				histories: JSON.parse(JSON.stringify(action.payload))
			};
        case SET_HISTORY_WALLET_TYPE:
            return{
				...state,
				walletFilterType: action.payload
			};
        
        case SET_HISTORY_TRANSFER_TYPE:
            return{
				...state,
				transferFilterType: action.payload
			};
		case SET_HISTORY_CURRENT_PAGE:
            return{
				...state,
				page: action.payload
			};
		case SET_HISTORY_CURRENT_PAGE_LIMIT:
            return{
				...state,
				limit: action.payload || 10,
			};

		case SET_HISTORY_FETCHING:
            return{
				...state,
				isFetching: action.payload
			};


		case SET_WALLETS_PAGING_RESULT:
            return{
				...state,
				page  : action.payload.page,
				limit : action.payload.limit,
				total : action.payload.total,
				pages : action.payload.pages,
			};
		default: return state;
    }
}

export default historyReducer;