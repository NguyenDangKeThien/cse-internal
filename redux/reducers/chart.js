import { SET_CHART_DATA } from '../actions/type.js';

const initialState = {
	labels: [],
	datasets: []
};

const chartReducer = (state = initialState, action) => {
    switch(action.type) {
        case SET_CHART_DATA:
            return{
				...state,
				labels: action.data.labels,
				datasets: action.data.datasets,
			};
		default: return {...state};
    }
}

export default chartReducer;