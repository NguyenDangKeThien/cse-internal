import {createStore, applyMiddleware, compose} from "redux";
import thunk from 'redux-thunk';
import reducer from '../reducers';
import window from 'global'

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = (initialState = {}) => {
    return createStore(reducer, initialState, composeEnhancer(applyMiddleware(thunk)));
};

export default store;