import React, { Component } from 'react';
import Router from 'next/router';
import Link from 'next/link';
import http from '../plugins/http';

// import Components
import Heading from '../components/client/Heading';
import Wallet from '../components/client/Wallet';
import Chart from '../components/client/Chart';
import TransferHistory from '../components/client/TransferHistory';
// Redux
import { connect } from 'react-redux';
import * as actions from '../redux/actions/wallet-actions';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.dropdownValue = [
            {
                name: 'hour',
                value: '1 Hour',
                smValue: '1H'
            },
            {
                name: 'day',
                value: '1 Day',
                smValue: '1D'
            },
            {
                name: 'week',
                value: '1 Week',
                smValue: '1W'
            },
            {
                name: 'month',
                value: '1 Month',
                smValue: '1M'
            }
        ]
        this.state = {
            dropdownOpen: false,
            dropDownValue: '1 Hour'
        }
    }

    toggleDropdown = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        })
    }

    changeValue = (e) => {
        let value = '';
        if (e.currentTarget.dataset && e.currentTarget.dataset.value) {
            value = e.currentTarget.dataset.value;
        } else {
            value = e.currentTarget.textContent;
        }
        this.setState({dropDownValue: value});
        const index = this.dropdownValue.findIndex((item) => {
            return item.value === value;
        })
        if (index !== -1) {
            this.props.fetchChartData(this.dropdownValue[index].name);
        }
    }

    componentDidMount(){
        this.props.fetchChartData(this.dropdownValue[0].name);
    }
    render() {
        return (
            <div className="">
                <div className="top-offer">
                    <div className="pb-3 not-display-mobile">
                        <div className="d-flex align-items-center">
                            <div className="img-hightlight">
                                <img src="./images-asset/hightlight.png" alt="" />
                            </div>
                            <div>
                                <h3 className="title-dash m-0 text-24">Currency Balance</h3>
                            </div>
                        </div>
                    </div>
                    <div className="pb-4"></div>
                    <Wallet />
                </div>

                <div className="chart pt-4">
                    <div className="justify-content-between mb-4 d-flex align-items-center not-display-mobile">
                        <Heading className="chart-title" heading='CSE Chart' />
                        <Dropdown className="dropdown-custom"
                        isOpen={this.state.dropdownOpen}
                        toggle={this.toggleDropdown}>
                            <DropdownToggle caret>
                                {this.state.dropDownValue}
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem onClick={this.changeValue}>{this.dropdownValue[0].value}</DropdownItem>
                                <DropdownItem onClick={this.changeValue}>{this.dropdownValue[1].value}</DropdownItem>
                                <DropdownItem onClick={this.changeValue}>{this.dropdownValue[2].value}</DropdownItem>
                                <DropdownItem onClick={this.changeValue}>{this.dropdownValue[3].value}</DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                    </div>

                    <div className="pt-4">
                        <div className="ml-3 mr-3 mb-4 mt-5 d-flex gl-custom-border d-md-none">
                            <div data-value={this.dropdownValue[0].value}
                            className={`col p-2 text-center ${this.state.dropDownValue === this.dropdownValue[0].value ? 'active' : ''}`}
                            onClick={this.changeValue}>
                                {this.dropdownValue[0].smValue}
                            </div>
                            <div data-value={this.dropdownValue[1].value}
                            className={`col p-2 text-center ${this.state.dropDownValue === this.dropdownValue[1].value ? 'active' : ''}`}
                            onClick={this.changeValue}>
                                {this.dropdownValue[1].smValue}
                            </div>
                            <div data-value={this.dropdownValue[2].value}
                            className={`col p-2 text-center ${this.state.dropDownValue === this.dropdownValue[2].value ? 'active' : ''}`}
                            onClick={this.changeValue}>
                                {this.dropdownValue[2].smValue}
                            </div>
                            <div data-value={this.dropdownValue[3].value}
                            className={`col p-2 text-center ${this.state.dropDownValue === this.dropdownValue[3].value ? 'active' : ''}`}
                            onClick={this.changeValue}>
                                {this.dropdownValue[3].smValue}
                            </div>
                        </div>
                    </div>

                    <div className="p-0 bgr-dash text-white">
                        <div>
                            <Chart labels={this.props.chart.labels} datasets={this.props.chart.datasets}/>
                        </div>
                    </div>
                </div>
                {/*<TransferHistory wallet={ this.props.wallet } router={ this.props.router }/>*/}
            </div>
        );
    }
}

const mapDisPatchToProps = (dispatch, props) => {
    return {
        fetchChartData: (data) => {
            dispatch(actions.fetchChartData(data));
        }
    }
};

const mapStateToProps = state => ({
    wallet: state.wallet,
    chart: state.chart
});

export default connect(mapStateToProps, mapDisPatchToProps)(HomePage);
