import React, { Fragment, Component } from 'react'
import Router from 'next/router';
import InitTitle from '../components/client/common/InitTitle';
import OtpInput from '../libs/otp-input';
import http from '../plugins/http';
import AuthService from '../services/auth.service';
import { toast } from 'react-toastify';
import { debounce } from "debounce";

// Redux
import * as actions from '../redux/actions';
import { connect } from 'react-redux';

class VerifyEmail extends Component {

    constructor(props) {
        super(props);
        this.state = {
          otp: [],
          numInputs: 6,
          formValid: false,
          formErrors: {otp: ''}
        };
    }

    handleOtpChange = value => {
        this.setState({formErrors: {otp: ''}});
        this.setState({otp: value}, this.validateForm);
    };

    validateForm() {
        const code = this.state.otp && this.state.otp.length && this.state.otp.join('');
        this.setState({formValid: code.length === 6});
    }

    renderErrors = () => {
        return <div className="mt-2 text-13 text-center text-danger">{ this.state.formErrors.otp }</div>;
    }

    resendCode = debounce( this.handleResendCode.bind(this), 1000)

    handleResendCode() {
        http.post('auth/reSendVerifyEmail', {
            email: this.props && this.props.user ? this.props.user.email : ''
        })
        .then(() => {
            toast.success('Success! An e mail has been sent');
        })
        .catch(() => {
            toast.error('Failed! Server has errors');
        });
    }

    submitForm = (event) => {
        event.preventDefault();
        if (this.state.formValid) {
            const userInfo = this.props.user;
            const isEmailVerified = userInfo && userInfo.isEmailVerified;
            if (isEmailVerified) {
                this.setState({formValid: false});
                return;
            }
            const data = {
                email: userInfo.email,
                codeVerify: this.state.otp.join('')
            }
            http.post('auth/verifyEmail', data)
                .then(() => {
                    AuthService.setAccessToken();
                    Router.push('/');
                })
                .catch((response) => {
                    this.setState({formValid: false});
                    if (response.data && response.data.message === 'CODE_VERIFY_NOT_MATCH') {
                        this.setState({formErrors : { otp: 'Verification code does not match' }})
                    }
                });
        }
    }

    render() {
        const { otp, numInputs } = this.state;
        return (
            <Fragment>
                <style 
                    jsx>{`
                        .bgr-login {
                            background-image: url('./images-asset/forgot-bg.png');
                            background-size: cover;
                            height: fit-content;
                            background-repeat: no-repeat;
                            min-height: 100vh;
                        }
                    `}
                </style>
                <div className="verify-email">
                    <div className="login__form bgr-login align-items-center text-white px-md-5 px-3 d-md-flex">
                        <div className="max-width-400 mx-auto w-100">
                            <div className="text-center logo">
                                <svg width="122" height="122" viewBox="0 0 122 122" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M121.975 46.6389C121.963 46.4988 121.935 46.3603 121.892 46.2262C121.864 46.1063 121.825 45.9893 121.776 45.8764C121.719 45.766 121.651 45.6617 121.573 45.5651C121.495 45.4505 121.404 45.345 121.303 45.2499C121.27 45.2215 121.256 45.1827 121.221 45.1543L101.666 29.9667V14.226C101.666 10.857 98.9344 8.12568 95.5654 8.12568H73.529L64.7057 1.27499C62.526 -0.424996 59.4694 -0.424996 57.2898 1.27499L48.4646 8.12568H26.4282C23.0592 8.12568 20.3279 10.8568 20.3279 14.226V29.9669L0.778504 45.1546C0.743952 45.1832 0.729654 45.2218 0.697246 45.2501C0.595972 45.345 0.505182 45.4508 0.426783 45.5654C0.348623 45.6619 0.280471 45.766 0.223519 45.8766C0.174192 45.9893 0.135589 46.1065 0.107709 46.2264C0.0652924 46.3587 0.0374121 46.495 0.0243059 46.6332C0.0243059 46.6758 0 46.7125 0 46.7552V115.898C0.00285952 117.191 0.421303 118.449 1.19361 119.487C1.20576 119.505 1.20791 119.527 1.22197 119.544C1.23627 119.56 1.26677 119.578 1.28702 119.601C2.4294 121.104 4.20612 121.99 6.09411 121.998H115.9C117.795 121.992 119.579 121.102 120.723 119.59C120.739 119.57 120.764 119.564 120.778 119.544C120.792 119.524 120.794 119.505 120.806 119.487C121.579 118.449 121.997 117.191 122 115.898V46.7609C122 46.7182 121.978 46.6815 121.975 46.6389ZM59.7766 4.48361C60.4867 3.91933 61.4923 3.91933 62.2024 4.48361L66.8935 8.12545H55.0996L59.7766 4.48361ZM6.60644 117.931L59.7768 76.63C60.4874 76.0667 61.4923 76.0667 62.2027 76.63L115.387 117.931H6.60644ZM117.933 114.761L64.7057 73.4212C62.5256 71.7226 59.4699 71.7226 57.2898 73.4212L4.06052 114.761V49.8578L37.3825 75.7333C38.2704 76.4218 39.5481 76.2602 40.2365 75.3723C40.925 74.4844 40.7634 73.2067 39.8755 72.5183L6.02286 46.2321L20.3281 35.1153V50.8276C20.3281 51.9507 21.2386 52.861 22.3614 52.861C23.4845 52.861 24.3948 51.9505 24.3948 50.8276V14.2258C24.3948 13.1027 25.3051 12.1924 26.4282 12.1924H95.5649C96.688 12.1924 97.5983 13.1027 97.5983 14.2258V50.8276C97.5983 51.9507 98.5086 52.861 99.6316 52.861C100.755 52.861 101.665 51.9505 101.665 50.8276V35.1153L115.97 46.2321L82.0587 72.5652C81.4753 73.0072 81.1744 73.7288 81.2711 74.4544C81.3679 75.18 81.8471 75.7977 82.526 76.0715C83.2049 76.3453 83.9786 76.2333 84.5517 75.7779L117.933 49.8578V114.761H117.933Z" fill="#3CC1C5"/>
                                    <path d="M85.3979 48.7942V40.6605C85.3979 27.1841 74.4731 16.2593 60.9967 16.2593C47.5202 16.2593 36.5954 27.1841 36.5954 40.6605C36.5954 54.137 47.5202 65.0618 60.9967 65.0618C62.1197 65.0618 63.03 64.1515 63.03 63.0284C63.03 61.9053 62.1197 60.995 60.9967 60.995C49.7664 60.995 40.6624 51.891 40.6624 40.6608C40.6624 29.4305 49.7664 20.3265 60.9967 20.3265C72.227 20.3265 81.331 29.4305 81.331 40.6608V48.7944C81.331 51.0406 79.5101 52.8614 77.264 52.8614C75.0178 52.8614 73.197 51.0406 73.197 48.7944V40.6608C73.197 39.5377 72.2868 38.6274 71.1637 38.6274C70.0406 38.6274 69.1303 39.5377 69.1303 40.6608C69.1303 45.1528 65.4887 48.7944 60.9967 48.7944C56.5046 48.7944 52.863 45.1528 52.863 40.6608C52.863 36.1687 56.5046 32.5271 60.9967 32.5271C62.1197 32.5271 63.03 31.6168 63.03 30.4937C63.03 29.3707 62.1197 28.4604 60.9967 28.4604C55.5362 28.4482 50.7324 32.0658 49.2359 37.3173C47.7395 42.5688 49.9144 48.1751 54.5611 51.0432C59.2078 53.9113 65.1942 53.3427 69.2178 49.6509C69.6605 53.9511 73.3982 57.1485 77.7151 56.92C82.0322 56.6912 85.4115 53.1173 85.3979 48.7942Z" fill="#3CC1C5"/>
                                </svg>
                            </div>
                            <div className="login-title text-center pt-2">
                                <InitTitle title='Verify your email' subTitle='Please enter 6 digit code sent to your email' />
                            </div>
                            <form onSubmit={(e) => this.submitForm(e)}>
                                <div className="pt-4 code-input">
                                    <OtpInput
                                    inputStyle='form-control'
                                    numInputs={numInputs}
                                    onChange={this.handleOtpChange}
                                    shouldAutoFocus={true}
                                    value={otp}
                                    />
                                </div>
                                {this.renderErrors()}
                                <div className="btn-center pt-5">
                                    <button type="submit" className="btn btn-primary-custom w-90 gl-btn-auth my-3">
                                       Enter
                                    </button>
                                </div>
                                <div className="text-center py-2 ">
                                    <span className="cl-white opacity-dot6 font-weight-light">
                                        Not received email?
                                    </span>
                                    <span 
                                    className="cl-blue font-weight-bold cusor-pointer"
                                    onClick={this.resendCode}>
                                        &nbsp;Resend
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}
const mapDisPatchToProps = (dispatch, props) => {
    return {
        setUserData: (data) => {
            dispatch(actions.setUserData(data));
        }
    }
};

const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

export default connect(mapStateToProps, mapDisPatchToProps)(VerifyEmail);
