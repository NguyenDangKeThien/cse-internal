import React, { Fragment, Component } from 'react'
import { Input } from "reactstrap";
import Router from 'next/router';
import InitTitle from '../components/client/common/InitTitle';
import http from '../plugins/http';
import { toast } from 'react-toastify';

// Redux
import * as actions from '../redux/actions';
import { connect } from 'react-redux';

class NewPassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            password: '',
            confirmPassword: '',
            formErrors: {passwordValid: '', confirmPasswordValid: ''},
            confirmPasswordValid: false,
            formValid: false,
            passwordValid: false
        };
    }

    handleUserInput (e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value}, () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let confirmPasswordValid = this.state.confirmPasswordValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'password':
                if (!value) {
                    passwordValid = false;
                    fieldValidationErrors.password = 'You must enter password'
                } else {
                    passwordValid = (value.length >= 6 && value.length <= 15);
                    fieldValidationErrors.password = passwordValid ? '' : 'Password must be 6 to 15 characters';

                    confirmPasswordValid = this.state.confirmPassword ? ( value === this.state.confirmPassword) : true;
                    fieldValidationErrors.confirmPassword =  confirmPasswordValid ? '' : 'Re-password incorrectly';
                }
                break;
            case 'confirmPassword':
                if (!value) {
                    confirmPasswordValid = false;
                    fieldValidationErrors.confirmPassword = 'You must enter re-password'
                } else {
                    confirmPasswordValid = this.state.password ? ( value === this.state.password) : true;
                    fieldValidationErrors.confirmPassword =  confirmPasswordValid ? '' : 'Re-password incorrectly';
                }
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            passwordValid,
            confirmPasswordValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({
            formValid: this.state.passwordValid
                    && this.state.confirmPassword
                    && this.state.confirmPasswordValid
        });
    }

    renderErrors = (field) => {
        return <div className="mt-2 text-13 text-left text-danger">{ this.state.formErrors[field] }</div>;
    }

    submitForm = (event) => {
        event.preventDefault();
        if (!this.state.formValid) {
            const { password, confirmPassword } = this.state;
            const formFields = {
                password,
                confirmPassword
            }
            for (const field in formFields) {
                this.validateField(field, formFields[field]);
            }
            return;
        }
        if (this.state.formValid) {
            const data = {
                email: this.props.user.email,
                code: this.props.user.code,
                newPassword: this.state.password
            }
            http.post('auth/reset/password/confirm', data)
            .then(() => {
                toast.success('Success! Your password have been changed');
                Router.push('/login', '/login', { shallow: true });
            })
            .catch((response) => {
                this.setState({formValid: false});
                toast.error('Failed! Server has errors');
            });
        }
    }

    render() {
        return (
            <Fragment>
                <style 
                    jsx>{`
                        .bgr-login {
                            background-image: url('./images-asset/forgot-bg.png');
                            background-size: cover;
                            height: fit-content;
                            background-repeat: no-repeat;
                            min-height: 100vh;
                        }
                    `}
                </style>
                <div className="verify-email">
                    <div className="login__form bgr-login align-items-center text-white px-md-5 px-3 d-md-flex">
                        <div className="max-width-400 mx-auto w-100">
                            <div className="login-title text-center pt-2">
                                <InitTitle title='Add new password' subTitle='Please enter your new password' />
                            </div>
                            <form onSubmit={(e) => this.submitForm(e)}>
                                <div className="pt-4">
                                    <div className="gl-form-title">Password</div>
                                    <div className="input-icon">
                                    <Input
                                    className="login-input text-white border-0"
                                    name="password"
                                    type="password"
                                    value={this.state.password}
                                    onChange={(event) => this.handleUserInput(event)}/>
                                    </div>
                                    {this.renderErrors('password')}

                                    <div className="gl-form-title">Confirm password</div>
                                    <Input
                                    className="login-input text-white border-0"
                                    name="confirmPassword"
                                    type="password"
                                    value={this.state.confirmPassword}
                                    onChange={(event) => this.handleUserInput(event)}/>
                                    {this.renderErrors('confirmPassword')}
                                </div>
                                <div className="btn-center pt-4">
                                    <button type="submit" className="btn btn-primary-custom w-100 gl-btn-auth my-3">
                                       Enter
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}
const mapDisPatchToProps = (dispatch, props) => {
    return {
        setUserData: (data) => {
            dispatch(actions.setUserData(data));
        }
    }
};

const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

export default connect(mapStateToProps, mapDisPatchToProps)(NewPassword);
