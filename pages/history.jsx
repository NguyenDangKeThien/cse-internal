import React, { Component } from 'react';
import Link from 'next/link';
import { connect }            from 'react-redux';
import { bindActionCreators } from "redux";

// Import components
import SwiperCards from '../components/client/SwiperCards';
import Heading from '../components/client/Heading';
import TransferHistory from '../components/client/TransferHistory';
import { fetchWalletData } from '../redux/actions/wallet-actions';
import { fetchHistoryData, setHistoryData } from '../redux/actions/history-actions';

class History extends Component {
    state = {
        wallet : 'CSE'
    }
    selectWallet = (wallet) => {
        if(wallet === 'CSE')
            this.props.fetchHistoryData();
        else
            this.props.setHistoryData([]);
        this.setState({wallet : wallet})
    }
    componentDidMount(){
        this.props.fetchWalletData();   
    }
    render() {
        return (
            <div className="">
                <div className="top-offer">
                    <div className="pb-3">
                        {window.innerWidth > 768 && (
                            <div className="d-flex align-items-center">
                                <div className="img-hightlight">
                                    <img src="./images-asset/hightlight.png" alt="" />
                                </div>
                                <div>
                                    <h3 className="title-dash m-0 text-24">My Wallet</h3>
                                </div>
                            </div>
                        )} 
                    </div>
                    <SwiperCards currentSlide={1} selectSlide={this.selectWallet} walletsInfo={this.props.walletReducer}></SwiperCards>
                </div>
                <TransferHistory wallet={this.state.wallet} router={ this.props.router }/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    walletReducer: state.walletReducer,
});
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    { fetchWalletData, setHistoryData, fetchHistoryData },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(History);
