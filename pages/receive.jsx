import React, { Component } from 'react';
import Link from 'next/link';
import Dropdown from '../components/client/Dropdown';
import {formatNumber} from '../helper/utils';
import {
    Button,
    FormGroup,
    Input,
} from "reactstrap";
import { connect } from 'react-redux';
import { toast }   from 'react-toastify';

class Receive extends Component {
    state = {
        isTextCopied : false,
        walletQRCode : "./images-asset/qrcode.png",
    }
    coppyClipboard = (value) => {
        const input = document.createElement('textarea');
        input.innerHTML = value;
        document.body.appendChild(input);
        input.select();
        input.setSelectionRange(0, 99999); /*For mobile devices*/
        document.execCommand('copy');
        document.body.removeChild(input);
        this.setState({isTextCopied : true}, () => {
            setTimeout(() => { this.setState({isTextCopied : false})}, 5000);
        })
    }

    getNotifyDom = (notify) => {
        return (<div className="align-items-center">
            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M10 0C4.48578 0 0 4.48578 0 10C0 15.5142 4.48578 20 10 20C15.5142 20 20 15.5142 20 10C20 4.48578 15.5142 0 10 0Z" fill="#3CC1C5"/>
                <path d="M15.0682 7.8808L9.65152 13.2974C9.48901 13.4599 9.2757 13.5417 9.06238 13.5417C8.84906 13.5417 8.63574 13.4599 8.47324 13.2974L5.76495 10.5891C5.43903 10.2633 5.43903 9.73657 5.76495 9.4108C6.09073 9.08487 6.61731 9.08487 6.94324 9.4108L9.06238 11.5299L13.89 6.70251C14.2157 6.37659 14.7423 6.37659 15.0682 6.70251C15.394 7.02829 15.394 7.55487 15.0682 7.8808Z" fill="#FAFAFA"/>
            </svg>
            <span className="overflow-hidden" style={{ color : "white", marginLeft: '10px', marginRight: '10px', fontSize: '13px', maxWidth: '230px'}}>{ notify }</span>
        </div>);
    }

    mobileRender = (cseWallet) => {
        return (<div className="cse-mobile-send-form-custom">
            <div className="mb-4 d-flex gl-custom-border d-lg-none">
                <Link href="/receive" >
                    <div className="col p-2 text-center active">
                        Receive
                    </div>
                </Link>
                <Link href="/send" >
                    <div className="col p-2 text-center">
                        Send
                    </div>
                </Link>
            </div>
            <div className="py-3" style={{ textAlign : "-webkit-center"}}>
                <div className="spacing-qrcode">
                    <div className="mx-auto qrcode bg-white">
                        <img width="290" height="290" src={`https://api.qrserver.com/v1/create-qr-code/?size=290x290&data=${cseWallet.address}`} alt="" />
                    </div>
                </div>
            </div>
            <div className="position-relative">
                <div className="d-flex align-items-center justify-content-between bdr-10 form-qr-code mt-2 disabled">
                    <Input className="reset-input pr-1 pl-3" value={cseWallet.address} disabled={ true }/>
                    <div>
                        <div className="cusor-pointer mr-3" style={{ width: 20 }}>
                            <img src="./images-asset/save.png" alt="" onClick={() => this.coppyClipboard(cseWallet.address)}/>
                        </div>
                    </div>
                </div>
                {this.state.isTextCopied ? <p className="text-copy-success text-right">Address wallet has been copied !</p> : ''}
            </div>
            {/*<div className="text-center mt-5 pt-4">
                <button type="button" className="btn btn-primary-custom cse-receive-button">
                    Request Send
                </button>
            </div>*/}
        </div>)
    }
    webRender = (cseWallet) => {
        return (<div>
            <div className="pb-3">
                <div className="d-flex align-items-center">
                    <div className="img-hightlight">
                        <img src="./images-asset/hightlight.png" alt="" />
                    </div>
                    <div>
                        <h3 className="title-dash m-0 text-24">Receive</h3>
                    </div>
                </div>
            </div>
            <div className="bdr-10 rs-box-shadow post-info__content position-relative p-3 min-h-cc cse-send-form-custom">
                <div className='text-white text-center send-info send-page bdr-10'>
                    <div className="py-3 pt-lg-5 mt-lg-5 pb-lg-5 mb-lg-5" style={{ textAlign : "-webkit-center"}}>
                        <div className="spacing-qrcode mb-4 ml-5">
                            <div className="mx-auto qrcode bg-white">
                                <img width="235" height="235" src={`https://api.qrserver.com/v1/create-qr-code/?size=235x235&data=${cseWallet.address}`} alt="" />
                            </div>
                        </div>
                        <div className="d-md-flex d-block align-items-center py-3">
                            <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15">To Wallet </div>
                            <Dropdown dropName={ 'wallet' } />
                        </div>

                       {/* <div className="d-md-flex d-none align-items-center py-3">
                            <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15"> Currency </div>
                            <Dropdown dropName={ 'currency' } />
                        </div>*/}

                        <div className="d-md-flex d-block py-3">
                            <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15 mt-3"> Address </div>
                            <div className="col-md-9 col-12 p-lg-0 position-relative">
                                <div className="d-flex align-items-center justify-content-between bdr-10 form-qr-code">
                                    <Input className="reset-input p-0 pl-3" value={cseWallet.address} disabled={ true }/>
                                    <div>
                                        <div className="cusor-pointer mr-3" style={{ width: 20 }}>
                                            <img src="./images-asset/save-white.png" alt="" onClick={() => this.coppyClipboard(cseWallet.address)}/>
                                        </div>
                                    </div>
                                </div>
                                {this.state.isTextCopied ? <p className="text-copy-success text-right m-0 position-absolute" style={{right: '-30px'}}>Address wallet has been copied !</p> : ''}
                            </div>
                        </div>
                        <div className="d-md-flex d-block align-items-center py-3">
                            <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15 cse-title-fee" > Balance </div>
                            <div className="text-16 ml-0 cse-text-fee" > {formatNumber(cseWallet.availableAmount)} CSE </div>
                            {/*<button type="button" className="btn w-100 btn-primary-custom cse-send-button">
                                Request Send
                            </button>*/}
                        </div>
                    </div>
                </div>
            </div>
        </div>)
    }
    render() {
        let cseWallet = this.props.walletReducer.wallets.find(wallet => wallet.coinAsset === 'CSE') ;
        cseWallet = cseWallet === undefined ? {coinAsset : 'CSE', availableAmount: 0, estimatedUSDT: 0, address: ''} : cseWallet;
        return (
            <div className={window.innerWidth <= 768 ? "main-introduction pl-4 pr-4" : "main-introduction"}>
                { window.innerWidth <= 768 ? this.mobileRender(cseWallet) : this.webRender(cseWallet) }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    walletReducer: state.walletReducer,
});

export default connect(mapStateToProps)(Receive);