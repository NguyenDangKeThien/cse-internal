import App from 'next/app';
import Router from 'next/router';
import { ToastContainer } from 'react-toastify';

// Import layouts
import Client from '../layouts/Client';

// Redux
import { Provider } from 'react-redux';
import initRedux from 'next-redux-wrapper';
import reduxStore from '../redux/store';

class AppLayout extends App {
    render() {
        const { Component, pageProps, store, router } = this.props;
        return (
            <Provider store={ store }>
                <ToastContainer />
                <Client router={ router }>
                    <Component router={ router } />
                </Client>
            </Provider>
        );
    }
}
export default initRedux(reduxStore, { debug: false })(AppLayout);