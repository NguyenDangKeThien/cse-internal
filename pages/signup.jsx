import React, { Component, Fragment } from 'react'
import Link from 'next/link';
import Router from 'next/router';
import { Button, Input, Row } from 'reactstrap';
import AuthService from '../services/auth.service';
import { toast } from 'react-toastify';
import http from '../plugins/http';
// Redux
import * as actions from '../redux/actions';
import { connect } from 'react-redux';

// import Components
import InitTitle from '../components/client/common/InitTitle';

class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            email: '',
            password: '',
            rePassword: '',
            formErrors: {
                username: '',
                email: '',
                password: '',
                rePassword: ''
            },
            usernameValid: false,
            emailValid: false,
            passwordValid: false,
            rePasswordValid: false,
            formValid: false
        }
    }

    handleUserInput (e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value}, () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let usernameValid = this.state.usernameValid;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
        let rePasswordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'username':
                if (!value) {
                    usernameValid = false;
                    fieldValidationErrors.username = 'You must enter user name'
                } else {
                    usernameValid = value.length > 5;
                    fieldValidationErrors.username = usernameValid ? '' : 'User name must be more than 6 characters';
                }
                break;
            case 'email':
                if (!value) {
                    emailValid = false;
                    fieldValidationErrors.email = 'You must enter email'
                } else {
                    emailValid = value.match(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
                    fieldValidationErrors.email = emailValid ? '' : 'Email is incorrect';
                }
                break;
            case 'password':
                if (!value) {
                    passwordValid = false;
                    fieldValidationErrors.password = 'You must enter password'
                } else {
                    passwordValid = (value.length >= 6 && value.length <= 15);
                    fieldValidationErrors.password = passwordValid ? '' : 'Password must be 6 to 15 characters';
                }
                break;
            case 'rePassword':
                if (!value) {
                    rePasswordValid = false;
                    fieldValidationErrors.rePassword = 'You must enter re-password'
                } else {
                    rePasswordValid = value === this.state.password;
                    fieldValidationErrors.rePassword = rePasswordValid ? '' : 'Re-password incorrectly';
                }
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            usernameValid,
            emailValid,
            passwordValid,
            rePasswordValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({
            formValid: this.state.usernameValid && this.state.emailValid && this.state.passwordValid && this.state.rePasswordValid
        });
    }

    renderErrors = (field) => {
        return <div className="mt-2 text-13 text-left text-danger">{ this.state.formErrors[field] }</div>;
    }

    submitForm = (event) => {
        event.preventDefault();
        if (!this.state.formValid) {
            const { username, email, password, rePassword } = this.state;
            const formFields = {
                username,
                email,
                password,
                rePassword
            }
            for (const field in formFields) {
                this.validateField(field, formFields[field]);
            }
            return;
        }
        if (this.state.formValid) {
            const data = {
                username: this.state.username,
                email: this.state.email,
                password: this.state.password,
                rePassword: this.state.rePassword,
                sponsor: 'cseref',
            }
            http.post('auth/register', data)
            .then(async(response) => {
                // toast.success('Sign up success!');
                AuthService.setResponseToken(response.data.result.token);
                const user = await http.get('auth/me');
                const userInfo = user.data.result && user.data.result.user;
                const isEmailVerified = userInfo && userInfo.isEmailVerified;
                this.props.setUserData(userInfo);
                if (!isEmailVerified) {
                    Router.push('/verify-email', '/verify-email', { shallow: true });
                } else {
                    AuthService.setAccessToken(response.data.result.token);
                    Router.push('/');
                }
            })
            .catch(response => {
                if (response && response.data) {
                    this.setState({formValid: false});
                    if (response.data.message === 'USERNAME_EXIST') {
                        this.setState({
                            formErrors: {
                                ...this.state.formErrors,
                                username: 'User name already exists'
                            }
                        })
                    } else if (response.data.message === 'EMAIL_EXIST') {
                        this.setState({
                            formErrors: {
                                ...this.state.formErrors,
                                email: 'Email already exists'
                            }
                        })
                    }
                }
            })
        }
    }

    render() {
        return (
            <Fragment>
                <style 
                    jsx>{`
                        .bgr-login {
                            background-image: url('./images-asset/background-login.png');
                            background-size: cover;
                            height: fit-content;
                            background-repeat: no-repeat;
                            min-height: 100vh;
                        }
                    `}
                </style>
                <div className="login">
                    <div className="login__form bgr-login d-flex align-items-center text-white px-md-5 px-3">
                        <div className="max-width-400 pl-80 mx-md-0 mx-auto w-100">
                            <div className="login-title text-center">
                                <InitTitle classBonus="title-desktop" title='Sign Up Now!' subTitle='Please enter your data to register.' />
                                <InitTitle classBonus="title-mobile" title='Sign UP' subTitle='Become our member' />
                            </div>
                            <form onSubmit={(e) => this.submitForm(e)}>
                                <div className="py-4">
                                    <div className="gl-form-title">
                                        <span className="text-danger">* &nbsp;</span>User name
                                    </div>
                                    <Input 
                                    className="login-input text-white border-0"
                                    type="text"
                                    name="username"
                                    value={this.state.username}
                                    onChange={(event) => this.handleUserInput(event)}/>
                                    {this.renderErrors('username')}

                                    <div className="gl-form-title">
                                        <span className="text-danger">* &nbsp;</span>Email
                                    </div>
                                    <Input 
                                    className="login-input text-white border-0"
                                    type="text"
                                    name="email"
                                    value={this.state.email}
                                    onChange={(event) => this.handleUserInput(event)}/>
                                    {this.renderErrors('email')}

                                    <div className="gl-form-title">
                                        <span className="text-danger">* &nbsp;</span>Password
                                    </div>
                                    <Input 
                                    className="login-input text-white border-0"
                                    type="password"
                                    name="password"
                                    value={this.state.password}
                                    onChange={(event) => this.handleUserInput(event)}/>
                                    {this.renderErrors('password')}

                                    <div className="gl-form-title">
                                        <span className="text-danger">* &nbsp;</span>Confirm Password
                                    </div>
                                    <Input 
                                    className="login-input text-white border-0"
                                    type="password"
                                    name="rePassword"
                                    value={this.state.rePassword}
                                    onChange={(event) => this.handleUserInput(event)}/>
                                    {this.renderErrors('rePassword')}
                                </div>
                                <div className="btn-center">
                                    <button
                                    type="submit"
                                    className="btn btn-primary-custom w-100 gl-btn-auth my-3">
                                        Sign up
                                    </button>
                                </div>
                            </form>
                            <div className="text-center py-2 cusor-pointer">
                                <Link href="/login">
                                    <span className="cl-blue font-weight-bold">
                                        Sign in here
                                    </span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

const mapDisPatchToProps = (dispatch, props) => {
    return {
        setUserData: (data) => {
            dispatch(actions.setUserData(data));
        }
    }
};

export default connect(null, mapDisPatchToProps)(SignUp);