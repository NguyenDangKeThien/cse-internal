import React, { Component, Fragment } from 'react';
import Router from 'next/router';
import Link from 'next/link';
import axios from "axios";
import { connect }            from 'react-redux';
import { bindActionCreators } from "redux";
import { Button, FormGroup, Input, } from "reactstrap";
import { fetchWalletData, selectWallet } from '../redux/actions/wallet-actions';
import Dropdown from '../components/client/Dropdown';
import SwiperCards from '../components/client/SwiperCards.jsx'
import AuthService from '../services/auth.service';
import { toast } from 'react-toastify';
import {formatNumber} from '../helper/utils';

class Send extends Component {
    constructor(props) {
        super(props);
        this.state = {
            toAddressError: '',
            amountError: '',
            secondFACodeError: '',
            address : '',
            coinAsset : 'CSE',
            amount : '',
            code2FA : '',
            usdAmount : '',
            warningMessage: ''
        };
    }

    componentDidMount(){
        this.props.fetchWalletData();   
    }

    handle2FAInput = (event) => {
        this.setState({
            code2FA: event.target.value, 
            secondFACodeError : '',
        })    
    }

    handleAddressInput = (event) => {
        this.setState({
            address: event.target.value, 
            toAddressError : '',
        })    
    }

    validateAllField = () => {
        this.setState({
            toAddressError: '',
            amountError: '',
            secondFACodeError: '',
            warningMessage : ''
        })
        let success = true;
        if(!this.state.address){
            this.setState({toAddressError: `Field address is required.`})
            success = false;
        }
        if(!this.state.amount){
            this.setState({amountError: `Field amount is required.`})
            success = false;
        }
        if(parseFloat(this.state.amount) > parseFloat(this.props.walletReducer.wallets[0].availableAmount)){
            this.setState({ warningMessage: '- Send amount must be smaller than available amount'});
            success = false;
        }
        if(!this.state.code2FA){
            this.setState({secondFACodeError: `Field 2FACode is required.`})
            success = false;
        }
        return success;
    }

    sendCoin = () => {
        if(this.validateAllField()){
            axios.post('https://csewallet.io:6969/v1/wallet/transfer',{
                address: this.state.address,
                coinAsset: this.state.coinAsset,
                amount: this.state.amount,
                code2FA: this.state.code2FA
            }, {headers: { 'access-token': AuthService.accessToken }},)
            .then(response => { 
                console.log(response);
                toast(this.getNotifyDom(true,"You have sent error"));
            })
            .catch(error => {
                console.log(error.response)
                this.setState({ warningMessage: error.response.data.message })
                toast(this.getNotifyDom(error.response.data.success,"You have sent error"));
            });
        }
    }

    getNotifyDom = (isSuccess = true, notify) => {
        if(isSuccess)
            return (<div className="align-items-center">
                <svg width="30" height="35" viewBox="0 0 65 56" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M10 0C4.47715 0 0 4.47715 0 10V46C0 51.5228 4.47715 56 10 56H55C60.5228 56 65 51.5229 65 46V10C65 4.47715 60.5228 0 55 0H10ZM30.6015 37.3103L45.8138 22.017C46.7287 21.0967 46.7287 19.61 45.8138 18.6902C44.8985 17.7699 43.4196 17.7699 42.5047 18.6902L28.947 32.3201L22.9956 26.3368C22.0803 25.4166 20.6014 25.4166 19.6865 26.3368C18.7712 27.2566 18.7712 28.7438 19.6865 29.6636L27.2924 37.3103C27.7488 37.7691 28.3479 38 28.947 38C29.5461 38 30.1451 37.7691 30.6015 37.3103Z" fill="#50BFC3"/>
                </svg>
                <span style={{ color : "white", marginLeft: '10px'}}>{ notify }</span>
            </div>);
        else
            return (<div className="align-items-center">
                <svg width="30" height="30" viewBox="0 0 99 99" fill="none" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
                    <rect width="99" height="99" fill="url(#pattern0)"/>
                    <defs>
                    <pattern id="pattern0" patternContentUnits="objectBoundingBox" width="1" height="1">
                    <use href="#image0" transform="scale(0.00421941)"/>
                    </pattern>
                    <image id="image0" width="237" height="237" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAO0AAADtCAYAAABTTfKPAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAABdpSURBVHgB7d1/jBzlfcfx7zOzu3c+O+LcACa0xWuMowAB7P7TKCLxWgJimz84R0KqHMDnilS0KthUpAJaxWe1JaigYjtVi9pUPrcp/6QSRo1salN5KS7in8ZHkGgkfq3TimCC6nVtn293b+fJfOdu7vb29sfs7vx45nk+L+l0vrs94zj39vPMMzPPCAJlyfHC6OVqNi+sTF4KOSqElbcErZ3/al6QGJUkR72PyBoV879m5y9WqHyp2vjblYX7NvetVPLeCe/jkvt7XJBSlKSUJSGccq2WK20qFssEShIEibvyO1vzTkZsFILcQMUd7v8pGx0S+cYIe9Ui2l6VhaQpDtu27LdtQVMzs9XSl0++OUWQKEQbMw60nqVCxo1TunG6I+TGQeJsJ4Ro2/Fj5nhfX5GxS+uOFRFyjBBtxDhSmRVjlkWbHbIKUQTaSoTRtlJy36ayNr2eEXYREUcL0YbMOw6tD4/ZlrVZkhiLK9JmMUfbrOSOxEV3RH6lUrWLOD4OF6INAYd6pb5iXFh0n/tXWiAFJBztEm68xTrRkVqt5gb8VolgIIi2TyqG2kilaBsh4MEh2h5wqNOzKwqWTXtUDLWRqtE2ylhWkSznyBePvTFJEBiiDeDSzu0b7QzdJ4XYm9Qxaq/SEG0D7xi4Wqntx+jbHaLt4OIDWwsZW+xTfVRtJWXRLuDRN5sVR9b9a3GSoCVE24SnwDPOyB73b2bc/TBPKZXWaBuUHEn7v3wSU+dmiHaeH2uapsCdaBCtr7QiZ0+WL80cwdR5jvHR6harT6NofSVBYnKmWjU+XmOj1TVWn4bR+krZjD1507V0UEyaedGGkdHOPLR9jyPEhI6x+jSO1mfsMa9R0c6tBluHKcULTEEZEK2vlMnQfpPO9RoR7ZXxrXmS4nAaT930y6BoPe553klTzvNqHa3ux62dmBatz12smrj5xH/sJ41pG61JU+FWTI12Ximbs3dv+HGxSBrSLloeXSty5T5Jci8ZzPBo5whxoFKx9ut2a6BW0Zo+ujZCtAtKdUfuvu2100XShBbRYnRdDtEupdOxbuqj5ZVhIa2Xeb8lggWItqVSpVrbkvYVZotSjC+SkNI+g2AhoPxQLnvm3W98LdUzslSOtJgOd4eRtgt3kermL1j703gpZOqixXQ4GEQbSCqny6maHvPqMKbDECKeLp965647C5QiqYmWj1/d0zmnTLuyCSKXty1x6r/v+fo+SolURDuz694XpKADBBARd31kIi3hKn1M6107LEdecH85TtATHNP27Wilau9W+SoqZUdaXnCqyJFThGAhXmNDOefUmcJX8qQoJaOdu5XOOoUFJ0iG3MgLVKqGq1y0frCE64chWXlVw1UqWgQLilEyXGWiRbCgKOXCVSJaBAuKUyrcxKNFsJASyoSbaLQIFlJGiXATixbBQkolHm4i0fKVTnynDiFYSCc33NzLZwqFRK6DTyTaK3LkMC6cgHSTG3PZ+guUgNijrTy0fZ8gGiOAlBOCxt/9xtdjDzfWaDlYR9AEAehCyr1xb18T210+89ubniKIBe7yiVfdkVvi2qY1lpGWV4rn9yMG0JJtiZfjWlGOPFpeKcapHTDAaFwrypFHy7smEoIFI8iNQ0NO5LtfRBqtty8xtjkFk8SwMBXZQhQfx/LOidiILRlYiEpUuVKtbYpqa9boRlqJnRPBWJEe30YSLZ+PJRzHgtGiO74NfXqM87FqwPRYDVGcvw11pOXTOzgfC7DIPX97OOxpcqjR4vQOwDL5sKfJoU2PMS1WC6bHaglzmhzaSItpMUB7PE32rg4MQSjRYrUYoKv8ux/PhnLRxcDT4/ltYz4iUAqmx0oK5aKLwUdaaaXmEYEACRsdymYHPowcKNpLD24fJzwgCyA4QYVBH2I9ULS2RRhlAXqUzQy2aNt3tPOjbJ4AoCeOI/PvbSv0vSjV10LU/MOezxCiVRYWopTnLkrZ6/p5eHVfI23VGdlDCBZgEKO5XH+ngHqOlk/xOAKLTwCDEiT29HNdcs/RWo61izDKAoShr9G2p2NaPH+nPetLt5G44Uay3DeavkT1n7xFzs/eoaTgmDY1ej62zVAPeJR1p8Z5ggXi6jWUffhxL9pG9j1jJD87R9Vnn/Lem2Dk9k10zYO7aXj9TWStXOV9bvqnU1Q+cZwunDxO0JI/2k4E/YbeRtpd2/lyxTyBx77zLsrs/D0SIyvbvoaDrR36c3J+/iHFKe6Rds0jj9Kv7bi/7ddr5z6hXzz/DF12I4ZlehptbQqIz8taWIBakBn7FmU52Gyu4+vEyCqyf3szOe/8F8kL5ykuM9W69xaH6594ilbfe1/H19irVtFV92zj5994oy8sMTw0RJW/fu9sMciLAy9E4eqnRRxsZmxn8G9wR+Lck8/OHe9qhoO96u5tgV9/9QO7ve+BpaRDu4K+NlC0uPppUc/B+jQMt9dgffw96/7mHxaOe2HuKqmg1yQHita2ZOB/BXTGx699BevTKNx+g/UNr99Aa587iHAb2EIEms12jdY7zUOiQIbjFeLMPffRwDQId9BgfRzub078BcE8QYVP77+768PWu4+0uF/WC5ZXikOT4nDDCtbHp4lwjLvolxeudH3gepDpcYEMFnqwvhSGG3awPv49Ee4cvrSx215SHaM1fQEqsmB9KQmXjzvXPncokmB9CHfB6AfnOg+UHaM1eQEq8mB9iofrBztye9dDrYEh3DnV2fqeTl9vG63JC1CxBetTNFw/WL4sMS4I1/17t0Wh090/baOt160CGSj2YH2KhZtEsD7Tw3XP2dLQUG283dfbRmvi1DixYH2KhJtksD7jR1zHant+seUNA6btZcwX/Gce/iOyf+srpITpy1R99smBbjLo94YBFYJtxHcHffz898hElaq9utVNBC1HWpOmxmJ+dFMmWJbQiKtasIxHXL7dz0TtpsgtozVlauwHK1RcuY05XBWD9fFNBkaG22aKvCza8+Njo6asGvOUWMlgfTGFq3KwPhPDbbeKvCza3GylQAbgi/+VmhK3E3G4aQjWx+F2utFeN7yKPPo5a9lljcuidesO4ap4tfFWMKFc/B+XiMJNU7A+3iFjZQwXeqiiWqttbv5cq2PaAmnM29Np57cpdUION43B+r7wxNPm3NInReeRtjq+nf8Jy5PGBrofNmkhhZvmYFl2zXX0+W+aMU2uSzn64favrm383JJoZ+uyQBrjUTbRiyfCMGC4aQ/Wt3rMnGPbmbrY0fjxkmgtS2wmjVlpWHgKos9wdQmW8UZxOvzvCMISVmHJx40fSCKtj/A7bXWaOj2Gq1OwvqEbN5AJhFw6mC5Ea8LxrJy+TFoJGK6OwTLn8kUyQfNx7UK0tVmZJ83JmDcMj0WXcHUNls188D6ZYrpGW/xfL0QrLP2vguJn6yT5fJ3I+OE2PZpE52ArH77vPbXAFBZZGxd/PU8IeQcZoHroz0h+9ilpx7/xYX513F71OW2DZf8z8TSZRSz0ufAALumWLHgpSnfzt715NwpcfS3phu8J5lNb17gr5UpfVz2Az3542KhR1iMWF4m9+2l5Eaou6QwZhH+wdQ1XZxzsL//pMJloOFPP33jszbPe9NiERahmc4+hfFLPqbKmTA6Wzczam/j93DGtJcy5ArsBwk0P04P1CCfP77xobUFGLEK1gnDVh2DnCLK8Tr1oJclRMhjCVReCXSTl3Ix4PlrLyOlxI4SrHgTbbG7tSfD2MsOyGt8jyhWHVWU1INjWeIdGayVV8wQLMOImD8G2NzpKeas+6xh9PNsKwk0Ogu3synTdjVZYeYJlEG78eFNyBNuZI+ujlhDmPsqyG4QbHw6WnyYAnQl3kLWEO00maAvhRg/BBpexxFrLrXYtQUcINzoItjeOdM/Tmn5hRVAIN3wItnfSkXmLIDCEGx4E2ydhjbrRijxBYAh3cAh2EHIUI20fEG7/EOzg3GNagWPaPiDc3iHYULjnabEQ1TeEGxyCDQ2mx4NCuN0h2HAh2hAg3PYQbPgQbUgQ7nIINhqINkQIdxGCjQ6iDRnCRbBRQ7QRMDlcBBs9Pk9bJgidieEi2FiU+Twtoo0Ih1v57h/q+bS+Jgg2NmVMj6M2fZlmT/876ezci99HsDHC9Dhi/BS77M5vk86uunur91hNiIGUJXd67CDaiHjBPvw46W54/QZa+9xBhBsTy5F0liB0pgTrQ7jxkEJcwDFtBEwL1odwoyd4euy+KxGExtRgfRzudb//KEE0pHBXj6WkEkEoTA/Wd9Xd2+j6J54iCJ+UjrsQJXGeNgwIdimEGw0prZKVzWB6PCgE2xrCDZ8QTtm6TLkSQd8QbGcIN1y1Wq5krZ48WsYFFv1BsMFwuNc8uJtgYOVNxeLCZYwlgp4g2N5c/cBuhDsoKaf43dyT4KXzNkFgCLY/CHcwfGEFv/eiFZKmCAJBsINBuP3LZETjSIvpcRAINhwItz+21TA9tjISI20XCDZcCLd3M5V6id8L/xPTu+49j43LW0Ow0fnsh4fx9PdgyreceGM1/2LhhgFBDkbbFhBstDDiBiQXZ8PW4ucEVpCb2PeMIdgYINxAFvpciNbBCvISmbFvab/jhEo4XN4BA9oQouj/ciFa23aKBB4ONjO2kyBe1z/xNMJtY8XIcMn/tWj8AhajEKwKPn7+Gbpw8lWCBSV3EWqd/8GSnSsskkUyGIJVA0bcpWTD8SxbEq3jyNfJUAhWLQh3Uc62i40fL4lW2PIoGQjBqmnNI4/R8PqbyHSZoWyx8WPR/ALTjmsRrNrqly7Rz//4MZr54H0y1JLjWbZsN0YpnVfIEAhWffaqVXTDXx4ydsQVgorNn2sRrSiSARBsepgcrpS0bBBdFm3Nzml/XKtzsPywr9mX/o50Y2q4lerSRSi2LFrefoY0PvUjrl6jdbD8eM3ZE6/Q5RefJ91wuL+x7xnKrrmOjCBlkbeXaf50yycMSIe0Pa7NPfanpCM/WDl92fv4/0++6l2koBsO1pSN4hwSR1p9vmW0FXtokjTEo6y44UbSTXOwvguahjty+yZaeftG0p1t14qtPt8yWl2nyNYN60g37YL1cbif/O0h0s2qr36NtOZOjW9+9a1Sqy+1fQCXllPkEb0eDNUtWN/5o/+i3Y3muj/kq93UmLWNVscpMv+Q6yJosD7sEJEu7abGrG20Ok6RHfcHPegPucp6DdanU7iVD/W9QipjWW2nxqzj82kdQQdJM/UT6T4N3W+wPl3CvfjmG6QtyznS8cudvliloaJujwzhc5jys08pjQYN1pf2cMsnjlPt3CekqdIXj70x2ekFHaPlKbKQUq/R1v2Br/3gryhtwgrWl+Zw+c+uq1bXGjezur3Atki7yxqdn72Tqkv9wg7Wl8Zw+c+s8SjrVlvb3+0lXaPNTR6b0vGcLU+TZ4/+M6kuqmB9aQr3/17+kdYr4N0WoHxdo/UI2bX+NJo9+pLS4UYdrI/D5SBUxvfTnnvx+6Sz7LA4EuR1gaJdMflq0f0RKpGGvHAVXFGOK1gfB3Hh5HFSEQd79juPkeZK644WJ4O8MNhIy2SwfwXSaPalv6f66ddIFXEH6/v4+e8pFy7/eThY5/Il0pkjKfBs1g76wic23Tplk/OIIBomDTk/ecu7ocBK+IaCsIKdqda9t15dfPM05a67jobXb6CkcbD8D4msVUlzpVtPvhH4EQuBR1otT/80qf3ghURH3KRG2GYqjLh+sCYYytk9zWKDT49dM1bugG4XWzRLKlxVgvVxMNM/PUNJMClYV6k6OzPZyzf0FK0Joy2LO1zVgvX97/4/ocqH71GcDAuWJMkjQU7zNOopWmbCaMviCpf/GyoGy3j70rPf2RNbuKYF6ypZ1uwk9ajnaE0ZbVnU4fLvzf8Nle88iitcA4MlOyN6HmVZz9EyU0ZbFlW4frBpEHW4JgZLfGPAtdYB6kNf0fJoawlHy6ukWvHCdU8JhSVNwfr8cMO+7tfQYL3zsmKy2NfAJ2gAV3Zt+8j9LfJkgpGVNPTkswNvDBdXsOcvVqh8Kfzzm7wb4trnDoWyjanBu2kse9RHL/oaaRcIGfiEcOq5x50VXjAaYMuaNI6wzXik5SuUBh1xTd7+JpOhgWapA0U7f01ykUzB4X730b6OcXUI1jdouCYHKwRNdrvJvZvBRlrvTyF3m7Io5eP4erk7SKdgff2GyzcmGL3BXID7ZbsZOFp3tC2ZcgqoEd8d5J1f7bJ1De9JpVuwvl7C5Tt1PvqD31X+FsAoSZL7+znF02yghSjf+fGx0WFZOWPMolQT+867vDfrS7d5H/N5V+9BWO5ozLtkJCGqhahW+Bk7q8fup89/8/5l+xFPvz1F5ZPHvE3TDTfQ4lOjUKJlV8a3FkhapwiUEGe0jfiRHTl3Zbl++ZI3utbO/YLAXXXP2Vs2/LhYpBCEFi2bfmj7AfdAew9B4pKKFlpwDx9vOXl6L4Vk8IWoBhUrN6HrDhcAfSpVapkJClGo0XpPJTDp3C1AF3VH7m71jNlBhBot43O3Uur3ZAKAXvFq8W2vnS5SyEKPlvE0WRJNEYC5SreeOD1BEYgkWu/2PeHsMO2iC4B5ZWHVtlBEIomW8UUXJt0JBLDA/bkP4yKKdiKLlg1PHj+A41swCp/e+bf/7Os+2aAijZbh+BYMUrr51zMTFLHIo8XxLRiixMex/d7Y3ovIo2XeTQWivoMANMXnY6M8jm0US7SMz98KIR8nAM1EdT62ndiiZViYAt1wsFGdj20n1mjZyD8e2+tIqe3DvMAoR+MOlsUeLataQ3uxogzpJqYqVTuR6+wTidZfUcYdQZBS7kpxdUfYNwIElUi0jFeUScgtCBdSxju1E9dKcSuJRcsQLqRM4sGyRKNlCBdSQolgWeLRMoQLilMmWKZEtAzhgqKUCpYpEy1DuKAY5YJlSkXLEC4oQslgmXLRMj9cXIAByRBTqgbLlIyWcbgVkeNwj/b0jQCDOVKpWsoGy0LdrDwqV8a3T5CkfQSBYbPyPoS8qXhUlB1pG62YPDbh/vOC/aYgOsJ5PA3BslREy+bCdbBABWEr1x25Jep9ncKUmmiZ9xBrrCxDaLwFp01x3sAehlRFy3iBakYMbcLN9DAQ9/hV9QWndlKxENXOzPi2vY609gmSowRLYCGqrTLvS5ym6XCzVEfLroxvzZMUp0x9oHU7iLYVng5Xd6RxdG2UuulxM54urzhyfB1Wl6GjFE+Hm6V+pG009zR6cRijLkbaBiXe3jRti02dpH6kbcSry1ikggXe6GqnbnW4G61G2kamj7qGj7Taja6NtI3WZ+olkIZGW5YkD95yfeZAHI/nSIr20TJeYXYcMWEJsYsMYVy0UhaFPbtbh4WmboyI1ufGO+5OmfeZMGU2JVrLEqXarKPtVLgVo6L1mRCvAdGWs7a9f8PxYmovkuiXVqvHQbmrzJO8yjx3bhfXMacMH7fud1eF15kYLDNypG3kXVFFljvyyl06jbwajrTeIlO1mjmQ1M7+qjA+Wp9u8WoULWJtgmhb0OGYN+3R8gKTIOvI9Awh1iaItoP5eHnkLVDKpDZa99RNXdJ+k1aDe4VoA1g8z0ub0zL6pixabwp8zVUrjl77o5PYgbMLRNujtIy+qkfrTn/JqTvFXCZzcP0aKup8BVPYEG2f5hauqKBqwMpG605/3bdXKrPZSRyr9gfRhkDFgFWJ1h9REWp4EG3I5HhhdJpWFMihsSSPgZOM1haiLIQ8ms3ar5cv0lGEGi5EG7FL49s3WiR5FN4sSG6MK+I4o+VI3X+uivW683pltu5Gqv9F+0lCtDHjiN1488KhghB0hyThfkyhb0wXYbRld6rLK7xvOySmarVaEZHGC9EqwA+ZHDdgN2TeXXLQmEOIlkfPkpRiSkjn7Vlpler12hQCTR6iVRgfH1+mkbxNzmidyB2drby3XawQaxu2jc17ryUx2hh5i2jL82/8ane05HOjopS1iWqOPOvGWXIcKn9uyC6Vp6mE41B1/QqFHLsUwunejAAAAABJRU5ErkJggg=="/>
                    </defs>
                </svg>
                <span style={{ color : "white", marginLeft: '10px'}}>{ notify }</span>
            </div>);
    }

    handleAmountInput = (event, availableAmount) => {
        if(event.target.value && !isNaN(event.target.value)){
            this.setState({
                amount: event.target.value, 
                usdAmount : this.calcUsdByCse(event.target.value),
                amountError: '',
            })
            let value = parseFloat(event.target.value);
            if(value > parseFloat(availableAmount)) 
                this.setState({ warningMessage: '- Send amount must be smaller than available amount'})
            else
                this.setState({ warningMessage: ''})
        }
    }

    handleUsdAmountInput = (event, availableAmount) => {
        if(event.target.value && !isNaN(event.target.value)){
            let amount = this.calcCseByUsd(event.target.value);
            this.setState({
                amount: amount, 
                usdAmount : event.target.value,
                amountError: '',
            })
            if(amount > parseFloat(availableAmount)) 
                this.setState({ warningMessage: '- Send amount must be smaller than available amount'})
            else
                this.setState({ warningMessage: ''})
        }
    }

    calcUsdByCse = (cse) => {
        const struct = this.props.walletReducer.wallets[0].estimatedUSDT / this.props.walletReducer.wallets[0].availableAmount
        return parseFloat(cse)*struct;
    }    
    
    calcCseByUsd = (usd) => {
        const struct = this.props.walletReducer.wallets[0].availableAmount / this.props.walletReducer.wallets[0].estimatedUSDT
        return parseFloat(usd)*struct;
    }

    renderErrors = (field) => {
        return <div className="mt-1 text-13 text-left text-danger">{ this.state[field] }</div>;
    }

    onBlurAmount = event => {
        this.setState({
            amount: parseFloat(this.state.amount), 
        })
    }

    onBlurAmountUsd = event => {
        this.setState({
            usdAmount: parseFloat(this.state.usdAmount), 
        })
    }

    mobileRender = (cseWallet) => {
        return (
            <div className="cse-mobile-send-form-custom">
                <div className="mb-4 d-flex gl-custom-border d-lg-none">
                    <Link href="/receive" >
                        <div className="col p-2 text-center">
                            Receive
                        </div>
                    </Link>
                    <Link href="/send" >
                        <div className="col p-2 text-center active">
                            Send
                        </div>
                    </Link>
                </div>

                <SwiperCards isSingle='CSE' selectSlide={this.props.selectWallet} walletsInfo={this.props.walletReducer}></SwiperCards>
                
                <div className="mobile-title-info text-center mt-4"> Send Money From </div>
                <div className="d-md-flex d-block align-items-center py-3">
                    <div className="d-flex align-items-center justify-content-between bdr-10 form-qr-code position-relative">
                        <Input name="address" value={this.state.address} onChange={(event) => this.handleAddressInput(event)} placeholder="Address" className="reset-input text-white text-white pr-2" />
                        <div>
                            <div className="cusor-pointer mr-3" style={{ width: 24, height: 24 }}>
                                <img src="./images-asset/scan1.png" alt="" />
                            </div>
                        </div>
                    </div>
                     {this.renderErrors('toAddressError')}
                </div>
                <div className="login__form">
                    <div className="bdr-10">
                        <Input onBlur={this.onBlurAmount} name="amount" value={this.state.amount} onChange={(event) => this.handleAmountInput(event, cseWallet.availableAmount)} className="text-left send-ammonunt-input text-white px-2 border-none py-1" type="text" placeholder="CSE" />
                    </div>
                    <div className="m-3 d-flex align-items-center justify-content-center trade-icon">
                        <svg width="21" height="22" viewBox="0 0 21 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <g opacity="0.5">
                            <path d="M4.60802 11.0109L0.897608 14.7139C0.310626 15.3002 0.31011 16.2513 0.896435 16.8383C0.89681 16.8387 0.897233 16.8391 0.897608 16.8395L4.65309 20.5949C5.17529 21.142 6.01535 21.2339 6.6435 20.8128C7.31286 20.3226 7.4582 19.3827 6.96806 18.7133C6.92844 18.6592 6.88526 18.6078 6.83878 18.5595L6.32804 18.0487C6.08304 17.8039 5.7849 17.6188 5.45676 17.5079L4.99108 17.3051L19.1793 17.3051C19.9467 17.3336 20.6199 16.7976 20.7641 16.0433C20.8969 15.2244 20.3407 14.4528 19.5218 14.32C19.4358 14.3061 19.3489 14.2996 19.2619 14.3008L4.99108 14.3008L5.41921 14.1505C5.73589 14.0377 6.02333 13.8555 6.26044 13.6173L6.80123 13.0765C7.38807 12.49 7.38844 11.5389 6.80202 10.952C6.77287 10.9229 6.74255 10.8949 6.71109 10.8682C6.07872 10.3644 5.16651 10.4264 4.60802 11.0109Z" fill="white"/>
                            <path d="M14.3699 0.495114C14.3264 0.54018 14.2858 0.587922 14.2482 0.638058C13.8271 1.26621 13.919 2.10626 14.466 2.62846L14.9392 3.10165C15.1027 3.2645 15.2901 3.40129 15.4951 3.50725L16.0584 3.80017L2.05793 3.80017C1.29524 3.77041 0.623716 4.29871 0.473121 5.04699C0.352758 5.86785 0.920681 6.63087 1.74154 6.75119C1.81895 6.76255 1.89711 6.7678 1.97531 6.76701L16.0358 6.76701L15.5927 6.9698C15.3558 7.07941 15.1398 7.22935 14.9543 7.41295L14.4585 7.90867C13.9115 8.43087 13.8196 9.27093 14.2407 9.89908C14.7309 10.5684 15.6708 10.7138 16.3402 10.2236C16.3943 10.184 16.4457 10.1408 16.494 10.0944L20.2495 6.33888C20.8365 5.75256 20.837 4.80143 20.2507 4.21445C20.2503 4.21407 20.2499 4.21365 20.2495 4.21328L16.494 0.457794C15.8971 -0.118437 14.9461 -0.101726 14.3699 0.495114Z" fill="white"/>
                            </g>
                        </svg>
                    </div>
                    <div className="bdr-10">
                        <Input onBlur={this.onBlurAmountUsd} name="usdAmount" value={this.state.usdAmount} onChange={(event) => this.handleUsdAmountInput(event, cseWallet.availableAmount)} className="text-left send-ammonunt-input text-white px-2 border-none py-1" type="text" placeholder="USD" />
                    </div>
                </div>
                {this.renderErrors('amountError')}
                <div className="d-md-flex d-block align-items-center py-3">
                    <div className="d-flex align-items-center justify-content-between bdr-10 form-qr-code">
                        <Input name="code2FA" value={this.state.code2FA} onChange={(event) => this.handle2FAInput(event)} className="reset-input text-white pr-2" placeholder="Enter 2FA"/>
                    </div>
                    {this.renderErrors('secondFACodeError')}
                </div>
                <div className="mobile-title-info text-left mt-4"> Fee: 0.001 CSE </div>
                <div className="text-center py-5">
                    <button type="button" className="btn btn-primary-custom cse-send-button" onClick={this.sendCoin}>
                        Send Now
                    </button>
                </div>
                { !this.props.userReducer.isHave2Fa || this.state.warningMessage && (
                    <div className="account__footer text-left pt-3">
                        <div className="warning">
                            <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="12.1964" cy="12.1964" r="12.1964" fill="#FBBF23"/>
                            <path d="M12.124 6.48511C12.3949 6.48511 12.6058 6.56323 12.7568 6.71948C12.9131 6.87573 12.9912 7.07365 12.9912 7.31323C12.9912 7.54761 12.9131 7.74292 12.7568 7.89917C12.6058 8.05021 12.3949 8.12573 12.124 8.12573C11.8532 8.12573 11.6423 8.05021 11.4912 7.89917C11.3454 7.74292 11.2725 7.54761 11.2725 7.31323C11.2725 7.07365 11.3454 6.87573 11.4912 6.71948C11.6423 6.56323 11.8532 6.48511 12.124 6.48511ZM11.3818 18.0164V9.56323H12.8428V18.0164H11.3818Z" fill="white"/>
                            </svg>
                            <div className="pl-3 d-block">
                                {!this.props.userReducer.isHave2Fa && (<div className="m-0 mb-2">- Enable 2FA code before sending to protect your wallet 
                                    <Link href="/setting" ><span className="pl-3" style={{color: "#50BFC3", cursor: "pointer"}}>Go to Setting</span></Link>
                                </div>)}
                                <div className="m-0">{this.state.warningMessage}</div>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        )
    }

    webRender = (cseWallet) => {
        return (
            <div>
                <div className="pb-3">
                    <div className="d-flex align-items-center">
                        <div className="img-hightlight">
                            <img src="./images-asset/hightlight.png" alt="" />
                        </div>
                        <div>
                            <h3 className="title-dash m-0 text-24">Send</h3>
                        </div>
                    </div>
                </div>
                
                <div className="bdr-10 rs-box-shadow post-info__content position-relative p-3 cse-send-form-custom">
                    <div className='text-white text-center send-info send-page bdr-10'>
                        <div className="py-3 pt-lg-5 mt-lg-5 pb-lg-5 mb-lg-5">
                           {/* <div className="d-md-flex d-block align-items-center py-3">
                                <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15"> Currency </div>
                                <Dropdown dropName={ 'currency' } />
                            </div>*/}

                            <div className="d-md-flex d-none align-items-center py-3">
                                <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15">From Wallet </div>
                                <Dropdown dropName={ 'wallet' } />
                            </div>

                            <div className="d-md-flex d-block align-items-center py-3">
                                <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15"> Available </div>
                                <div className="text-16 cl-blue ml-0"> {formatNumber(cseWallet.availableAmount)} </div>
                            </div>

                            <div className="d-md-flex d-block align-items-center py-3">
                                <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15"> To Address </div>
                                <div className="col-md-9 col-12 p-lg-0">
                                    <div className="d-flex align-items-center justify-content-between bdr-10 form-qr-code">
                                        <Input className="reset-input text-white pl-3 pr-3" name="address" value={this.state.address} onChange={(event) => this.handleAddressInput(event)} />
                                        <div>
                                            <div className="cusor-pointer mr-3" style={{ width: 24, height: 24 }}>
                                                <img src="./images-asset/qrIcon.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                {this.renderErrors('toAddressError')}
                                </div>
                            </div>
                            <div className="d-md-flex d-block align-items-center py-3">
                                <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15"> Amount </div>
                                <div className="col-md-9 col-12 p-lg-0 login__form">
                                    <div className="d-md-flex align-items-center justify-content-between py-2">
                                        <div className="bdr-10">
                                            <Input onBlur={this.onBlurAmount} type="number" name="amount" value={this.state.amount} onChange={(event) => this.handleAmountInput(event, cseWallet.availableAmount)} className="text-left pl-3 send-ammonunt-input text-white px-2 border-none py-1" type="text" placeholder="CSE" />
                                        </div>
                                        <div className="m-3 d-flex align-items-center justify-content-center trade-icon">
                                            <svg width="21" height="22" viewBox="0 0 21 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <g opacity="0.5">
                                                <path d="M4.60802 11.0109L0.897608 14.7139C0.310626 15.3002 0.31011 16.2513 0.896435 16.8383C0.89681 16.8387 0.897233 16.8391 0.897608 16.8395L4.65309 20.5949C5.17529 21.142 6.01535 21.2339 6.6435 20.8128C7.31286 20.3226 7.4582 19.3827 6.96806 18.7133C6.92844 18.6592 6.88526 18.6078 6.83878 18.5595L6.32804 18.0487C6.08304 17.8039 5.7849 17.6188 5.45676 17.5079L4.99108 17.3051L19.1793 17.3051C19.9467 17.3336 20.6199 16.7976 20.7641 16.0433C20.8969 15.2244 20.3407 14.4528 19.5218 14.32C19.4358 14.3061 19.3489 14.2996 19.2619 14.3008L4.99108 14.3008L5.41921 14.1505C5.73589 14.0377 6.02333 13.8555 6.26044 13.6173L6.80123 13.0765C7.38807 12.49 7.38844 11.5389 6.80202 10.952C6.77287 10.9229 6.74255 10.8949 6.71109 10.8682C6.07872 10.3644 5.16651 10.4264 4.60802 11.0109Z" fill="white"/>
                                                <path d="M14.3699 0.495114C14.3264 0.54018 14.2858 0.587922 14.2482 0.638058C13.8271 1.26621 13.919 2.10626 14.466 2.62846L14.9392 3.10165C15.1027 3.2645 15.2901 3.40129 15.4951 3.50725L16.0584 3.80017L2.05793 3.80017C1.29524 3.77041 0.623716 4.29871 0.473121 5.04699C0.352758 5.86785 0.920681 6.63087 1.74154 6.75119C1.81895 6.76255 1.89711 6.7678 1.97531 6.76701L16.0358 6.76701L15.5927 6.9698C15.3558 7.07941 15.1398 7.22935 14.9543 7.41295L14.4585 7.90867C13.9115 8.43087 13.8196 9.27093 14.2407 9.89908C14.7309 10.5684 15.6708 10.7138 16.3402 10.2236C16.3943 10.184 16.4457 10.1408 16.494 10.0944L20.2495 6.33888C20.8365 5.75256 20.837 4.80143 20.2507 4.21445C20.2503 4.21407 20.2499 4.21365 20.2495 4.21328L16.494 0.457794C15.8971 -0.118437 14.9461 -0.101726 14.3699 0.495114Z" fill="white"/>
                                                </g>
                                            </svg>
                                        </div>
                                        <div className="bdr-10">
                                            <Input onBlur={this.onBlurAmountUsd} name="usdAmount" value={this.state.usdAmount} onChange={(event) => this.handleUsdAmountInput(event, cseWallet.availableAmount)} className="text-left pl-3 send-ammonunt-input text-white px-2 border-none py-1" type="text" placeholder="USD" />
                                        </div>
                                    </div>
                                    {this.renderErrors('amountError')}
                                </div>
                            </div>
                            
                            <div className="d-md-flex d-block align-items-center py-3">
                                <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15"> 2FA </div>
                                <div className="col-md-9 col-12 p-lg-0">
                                    <div className="d-flex align-items-center justify-content-between bdr-10 form-qr-code">
                                        <Input name="code2FA" value={this.state.code2FA} onChange={(event) => this.handle2FAInput(event)} className="reset-input text-white pl-3 pr-3" placeholder="Enter 2FA"/>
                                    </div>
                                {this.renderErrors('secondFACodeError')}
                                </div>
                            </div>
                            
                            <div className="d-md-flex d-block align-items-center py-3">
                                <div className="col-md-3 col-12 title-info py-md-0 p-lg-0 py-2 text-15 cse-title-fee" > Fee </div>
                                <div className="text-16 ml-0 cse-text-fee" > 0.001 CSE </div>
                                <button type="button" className="btn w-100 btn-primary-custom cse-send-button" onClick={this.sendCoin}>
                                    Send Now
                                </button>
                            </div>
                                
                            { !this.props.userReducer.isHave2Fa || this.state.warningMessage && (
                                <div className="account__footer text-left pt-5">
                                    <div className="warning">
                                        <svg width="25" height="25" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="12.1964" cy="12.1964" r="12.1964" fill="#FBBF23"/>
                                        <path d="M12.124 6.48511C12.3949 6.48511 12.6058 6.56323 12.7568 6.71948C12.9131 6.87573 12.9912 7.07365 12.9912 7.31323C12.9912 7.54761 12.9131 7.74292 12.7568 7.89917C12.6058 8.05021 12.3949 8.12573 12.124 8.12573C11.8532 8.12573 11.6423 8.05021 11.4912 7.89917C11.3454 7.74292 11.2725 7.54761 11.2725 7.31323C11.2725 7.07365 11.3454 6.87573 11.4912 6.71948C11.6423 6.56323 11.8532 6.48511 12.124 6.48511ZM11.3818 18.0164V9.56323H12.8428V18.0164H11.3818Z" fill="white"/>
                                        </svg>
                                        <div className="pl-3 d-block">
                                            {!this.props.userReducer.isHave2Fa && (<div className="m-0 mb-2">- Enable 2FA code before sending to protect your wallet 
                                                <Link href="/setting" ><span className="pl-3" style={{color: "#50BFC3", cursor: "pointer"}}>Go to Setting</span></Link>
                                            </div>)}
                                            <div className="m-0">{this.state.warningMessage}</div>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        let cseWallet = this.props.walletReducer.wallets.find(wallet => wallet.coinAsset === 'CSE') ;
        cseWallet = cseWallet === undefined ? {coinAsset : 'CSE', availableAmount: 0, estimatedUSDT: 0} : cseWallet;
        return (
            <div className={window.innerWidth <= 768 ? "main-introduction pl-4 pr-4" : "main-introduction"}>
                { window.innerWidth <= 768 ? this.mobileRender(cseWallet) : this.webRender(cseWallet) }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    walletReducer: state.walletReducer,
    userReducer: state.user,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    { fetchWalletData, selectWallet },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Send);