import React, { Component, Fragment } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import { Input } from 'reactstrap';
import AuthService from '../services/auth.service';
import http from '../plugins/http';

// Import components
import InitTitle from '../components/client/common/InitTitle';

// Redux
import * as actions from '../redux/actions';
import { connect } from 'react-redux';

class Login extends Component {

    constructor (props) {
        super(props);
        this.state = {
          email: '',
          password: '',
          formErrors: {email: '', password: ''},
          showPassword: false,
          emailValid: false,
          passwordValid: false,
          formValid: false
        }
    }

    handleUserInput (e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value}, () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;

        switch (fieldName) {
            case 'email':
                if (!value) {
                    emailValid = false;
                    fieldValidationErrors.email = 'You must enter email'
                } else {
                    emailValid = value.match(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
                    fieldValidationErrors.email = emailValid ? '' : 'Email is incorrect';
                }
                break;
            case 'password':
                if (!value) {
                    passwordValid = false;
                    fieldValidationErrors.password = 'You must enter password'
                } else {
                    passwordValid = (value.length >= 6 && value.length <= 15);
                    fieldValidationErrors.password = passwordValid ? '' : 'Password must be 6 to 15 characters';
                }
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid,
            passwordValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid});
    }

    getVerificationCode(email) {
        http.post('auth/reSendVerifyEmail', {
                email
            })
            .then(() => {
                Router.push('/verify-email');
            })
            .catch(() => {});
    }

    submitForm = (event) => {
        event.preventDefault();
        if (!this.state.formValid) {
            const { email, password } = this.state;
            const formFields = {
                email,
                password
            }
            for (const field in formFields) {
                this.validateField(field, formFields[field]);
            }
            return;
        }
        if (this.state.formValid) {
            const data = {
                email: this.state.email,
                password: this.state.password
            }
            http.post('auth/login', data)
                .then(async (response) => {
                    AuthService.setResponseToken(response.data.result.token);
                    const user = await http.get('auth/me');
                    const userInfo = user.data.result && user.data.result.user;
                    const isEmailVerified = userInfo && userInfo.isEmailVerified;
                    this.props.setUserData(userInfo);
                    if (!isEmailVerified) {
                        this.getVerificationCode(userInfo.email);
                    } else {
                        AuthService.setAccessToken(response.data.result.token);
                        Router.push('/');
                    }
                })
                .catch((response) => {
                    if (response && response.data) {
                        this.setState({formValid: false});
                        if (response.data.message === 'EMAIL_DOES_NOT_EXIST') {
                            this.setState({
                                formErrors: {
                                    ...this.state.formErrors,
                                    email: 'Email does not exist'
                                }
                            })
                        } else if (response.data.message === 'INCORRECT_PASSWORD') {
                            this.setState({
                                formErrors: {
                                    ...this.state.formErrors,
                                    password: 'Password is incorrect'
                                }
                            })
                        }
                    }
                })
        }
    }

    renderErrors = (field) => {
        return <div className="mt-2 text-13 text-left text-danger">{ this.state.formErrors[field] }</div>;
    }

    showEyeIcon() {
        const isShowPassword = this.state.showPassword;
        if (isShowPassword) {
            return (
                <svg width="23" height="17" viewBox="0 0 23 17" fill="none" xmlns="http://www.w3.org/2000/svg" className="cusor-pointer"
                onClick={() => {this.setState({showPassword: false})}}>
                    <path d="M 11.4836 0.34682 C 6.44446 0.34682 2.16912 3.76406 0.578034 8.52602 C 2.16912 13.288 6.44446 16.7052 11.4836 16.7052 C 16.5228 16.7052 20.7982 13.2879 22.3893 8.52602 C 20.7982 3.76406 16.5228 0.34682 11.4836 0.34682 Z M 11.4836 13.9788 C 8.47211 13.9788 6.03083 11.5375 6.03083 8.52602 C 6.03083 5.51452 8.47211 3.07324 11.4836 3.07324 C 14.4952 3.07324 16.9364 5.51452 16.9364 8.52602 C 16.9364 11.5375 14.4952 13.9788 11.4836 13.9788 Z" fill="#3CC1C5"/>
                    <path d="M 13.8406 6.25577 C 15.1424 7.55751 15.1424 9.66802 13.8406 10.9698 C 12.5389 12.2715 10.4283 12.2715 9.12659 10.9698 C 7.82485 9.66802 7.82485 7.55751 9.12659 6.25577 C 10.4283 4.95403 12.5389 4.95403 13.8406 6.25577 Z" fill="#3CC1C5"/>
                </svg>
            )
        } else {
            return (
                <svg width="23" height="17" viewBox="0 0 23 17" fill="none" xmlns="http://www.w3.org/2000/svg" className="cusor-pointer"
                onClick={() => {this.setState({showPassword: true})}}>
                    <path d="M11.3336 1.88916C5.51499 1.88916 0.330088 7.93729 0.111926 8.19511C-0.0373086 8.37078 -0.0373086 8.62861 0.111926 8.8052C0.242257 8.95913 2.14813 11.1719 4.89641 12.9342L7.85626 9.97437C7.66359 9.52105 7.55593 9.02332 7.55593 8.50014C7.55593 6.41674 9.25023 4.72244 11.3336 4.72244C11.8569 4.72244 12.3545 4.83011 12.8079 5.02277L15.2416 2.58898C14.0554 2.16492 12.7531 1.88916 11.3336 1.88916Z" fill="#3CC1C5"/>
                    <path d="M22.5827 8.23095C22.4618 8.05622 20.4568 5.23427 17.0861 3.4153L19.6956 0.805847C19.8797 0.621684 19.8797 0.322286 19.6956 0.138122C19.5114 -0.0460408 19.212 -0.0460408 19.0278 0.138122L2.97261 16.1934C2.78845 16.3776 2.78845 16.677 2.97261 16.8612C3.06421 16.9537 3.18511 17 3.30601 17C3.42691 17 3.54777 16.9537 3.64034 16.8621L6.60111 13.9013C8.06025 14.6134 9.66956 15.1111 11.3336 15.1111C17.1523 15.1111 22.3372 9.063 22.5553 8.80518C22.6922 8.64178 22.7045 8.40661 22.5827 8.23095ZM11.3336 12.2778C10.4619 12.2778 9.66956 11.969 9.02924 11.4723L14.3057 6.19577C14.8025 6.83609 15.1113 7.62848 15.1113 8.50015C15.1113 10.5835 13.417 12.2778 11.3336 12.2778Z" fill="#3CC1C5"/>
                </svg>
            )
        }
    }

    render() {
        return(
            <Fragment>
                <style
                    jsx>{`
                        .bgr-login {
                            background-image: url('./images-asset/background-login.png');
                            background-size: cover;
                            height: fit-content;
                            background-repeat: no-repeat;
                            min-height: 100vh;
                        }
                    `}
                </style>
                <div className="login">
                    <div className="login__form bgr-login d-md-flex align-items-center text-white px-md-5 px-3">
                        <div className="max-width-400 pl-80 w-100">
                            <div className="login-title text-center pt-2">
                                <InitTitle classBonus="title-desktop" title='Sign in Now!' subTitle='Please enter your credentials to login.' />
                                <InitTitle classBonus="title-mobile" title='Sign in' subTitle='Use your Email to sign in' />
                            </div>
                            <form onSubmit={(e) => this.submitForm(e)}>
                                <div className="py-4">
                                    <div className="gl-form-title">Email</div>
                                    <div className="input-icon email-input">
                                        <Input
                                        className="login-input text-white border-0"
                                        type="text"
                                        name="email"
                                        value={this.state.email}
                                        onChange={(event) => this.handleUserInput(event)}/>
                                    </div>
                                    {this.renderErrors('email')}

                                    <div className="gl-form-title">Password</div>
                                    <div className="input-icon">
                                        <Input
                                        className="login-input text-white border-0"
                                        name="password"
                                        value={this.state.password}
                                        type={this.state.showPassword?'text':'password'} 
                                        onChange={(event) => this.handleUserInput(event)}/>
                                        {this.showEyeIcon()}
                                    </div>
                                    {this.renderErrors('password')}
                                </div>
                                <div className="btn-center">
                                    <button
                                    type="submit"
                                    className="btn btn-primary-custom w-100 gl-btn-auth my-3">
                                        Sign in
                                    </button>
                                </div>
                            </form>
                            <div className="text-center py-2 cusor-pointer">
                                <Link href="/forgot">
                                    <span className="cl-white opacity-dot6 font-weight-light">
                                        Forgot your password?
                                    </span>
                                </Link>
                            </div>
                            <div className="text-center py-2 cusor-pointer">
                                <Link href="/signup">
                                    <span className="cl-blue font-weight-bold">
                                        Create your account here
                                    </span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

const mapDisPatchToProps = (dispatch, props) => {
    return {
        setUserData: (data) => {
            dispatch(actions.setUserData(data));
        }
    }
};
export default connect(null, mapDisPatchToProps)(Login);