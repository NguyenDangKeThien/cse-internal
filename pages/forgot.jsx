import React, { Fragment, Component } from 'react'
import { Input } from "reactstrap";
import InitTitle from '../components/client/common/InitTitle';
import http from '../plugins/http';
import Router from 'next/router';
import { toast } from 'react-toastify';
// Redux
import * as actions from '../redux/actions';
import { connect } from 'react-redux';
class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            formErrors: {
                email: '',
            },
            emailValid: false,
            formValid: false
        }
    }

    handleUserInput (e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value}, () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;

        switch (fieldName) {
            case 'email':
                if (!value) {
                    emailValid = false;
                    fieldValidationErrors.email = 'You must enter email'
                } else {
                    emailValid = value.match(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
                    fieldValidationErrors.email = emailValid ? '' : 'Email is incorrect';
                }
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            emailValid,
        }, this.validateForm);
    }

    validateForm() {
        this.setState({
            formValid: this.state.emailValid
        });
    }

    renderErrors = (field) => {
        return <div className="mt-2 text-13 text-left text-danger">{ this.state.formErrors[field] }</div>;
    }

    submitForm = (event) => {
        event.preventDefault();
        if (!this.state.formValid) {
            const { email } = this.state;
            const formFields = {
                email
            }
            for (const field in formFields) {
                this.validateField(field, formFields[field]);
            }
            return;
        }
        if (this.state.formValid) {
            http.post('auth/reset/password', {email: this.state.email})
                .then(() => {
                    this.props.setUserData({email: this.state.email});
                    Router.push('/forgot-confirm-code', '/forgot-confirm-code', { shallow: true });
                }).catch((response) => {
                    this.setState({formValid: false});
                    if (response.data && response.data.message && response.data.message.includes('INVALID_EMAIL')) {
                        this.setState({formErrors : { email: 'Email is not valid' }})
                    } else {
                        toast.error('Failed! Server has errors');
                    }
                })
        }
    }

    render() {
        return (
            <Fragment>
                <style 
                    jsx>{`
                        .bgr-login {
                            background-image: url('./images-asset/forgot-bg.png');
                            background-size: cover;
                            height: fit-content;
                            background-repeat: no-repeat;
                            min-height: 100vh;
                        }
                    `}
                </style>
                <div className="verify-email">
                    <div className="login__form bgr-login align-items-center text-white px-md-5 px-3 d-md-flex">
                        <div className="max-width-400 mx-auto w-100">
                            <div className="login-title text-center pt-2">
                                <InitTitle classBonus="" title='Forgot password' subTitle='Please enter your data to register.' />
                            </div>
                            <form onSubmit={(e) => this.submitForm(e)}>
                                <div className="py-4">
                                    <div className="gl-form-title">
                                        Email
                                    </div>
                                    <Input className="login-input text-white border-0"
                                    type="text"
                                    name="email"
                                    value={this.state.email}
                                    onChange={(event) => this.handleUserInput(event)}/>
                                    {this.renderErrors('email')}
                                </div>
                                <div className="btn-center">
                                    <button type="submit" className="btn btn-primary-custom w-100 gl-btn-auth my-3">
                                       Enter
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}
const mapDisPatchToProps = (dispatch, props) => {
    return {
        setUserData: (data) => {
            dispatch(actions.setUserData(data));
        }
    }
};

export default connect(null, mapDisPatchToProps)(ForgotPassword);
