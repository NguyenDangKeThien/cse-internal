/**
 * @class AuthService
 * @static
 * @hideconstructor
 */
class AuthService{
    /**
    * @static
    * @description Gets access token from the localStorage
    * @return {string}
    */
    static get accessToken(){
        let token = process.browser ? localStorage.getItem('cseWalletToken') : '';
        if(token){
            return token;
        }
        return token;
    }

    /**
    * @static
    * @description Sets access token at the localStorage
    * @param {string} token
    * @return {void}
    */
    static setAccessToken(accessToken = this._responseToken){
        if(process.browser) {
            localStorage.setItem('cseWalletToken', accessToken);
            this._responseToken = '';
        }
    }

    static setResponseToken(accessToken){
        this._responseToken = accessToken;
    }

    static get responseToken() {
        return this._responseToken || '';
    }


    /**
    * @static
    * @return {boolean}
    */
    static get isAuth(){
        return this.accessToken;
    }

    /**
    * @static
    * @description Clear all token of the localStorage.
    * @returns {void}
    */
    static logOut(){
        if(process.browser) {
            localStorage.removeItem('cseWalletToken');
        }
    }

    /**
    * @static
    * @description Set user login
    * @returns {void}
    */
    static setUser(){
        let token = process.browser ? localStorage.getItem('cseWalletToken') : '';
        if(token){
            return JSON.parse(this.decodeJWT(token));
        }
        return {};
    }

    /**
    * Decode base64String and fix utf8 problem
    */
    static decodeJWT(token) {
        try {
            const base64Url = token.split('.')[1];
            const base64 = base64Url.replace('-', '+').replace('_', '/');
            return decodeURIComponent(atob(base64).split('').map(function(c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));
        }catch(error) {
            this.logOut();
            return '{}';
        }
    }
}

export default AuthService;
