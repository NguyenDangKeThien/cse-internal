import React, { Component } from 'react';
import Router from 'next/router';
import Link from 'next/link';

class Pagination extends Component {

    constructor(props) {
        super(props);
        this.state = {
            limit : 10,
        }
    }

    setPageLimit = (limit) => {
        this.setState({ limit : limit });
        this.props.limitLoad(limit);
    }

    nextPage = () => {
        if(this.props.page !== this.props.pages)
            return this.props.loadData(this.props.page + 1);
    }

    prevPage = () => {
        if(this.props.page > 1)
            return this.props.loadData(this.props.page - 1);
    }

    getMinByCurrent = (current, limit) => {
        return current*limit - limit + 1;
    }
    getMaxByCurrent = (current, limit) => {
        if(current*limit > this.props.total)
            return this.props.total;
        return current*limit;
    }

    render() {
        return (
            <div className="text-right pr-3 pt-4 pb-4 d-flex flex-end">
                <div className="gl-cus-dropdown-min hidden-xs">
                    <span className="hisotry-paging-content">Rows per page:  {this.state.limit} </span>
                    <svg className="p-cursor" width="14" height="9" viewBox="0 0 14 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M7.14481 8.68613C7.02465 8.84396 6.78724 8.84396 6.66728 8.68613L0.430975 0.481637C0.280849 0.284132 0.421879 2.86779e-06 0.670037 2.8461e-06L13.1528 1.75482e-06C13.4009 1.73313e-06 13.5416 0.284131 13.3912 0.481636L7.14481 8.68613Z" fill="#50BFC3"/>
                    </svg>
                    <div class="gl-cus-dropdown-min__content text-center">
                        <div onClick={() => this.setPageLimit(100)} class="p-3 items mt-3">100</div>
                        <div onClick={() => this.setPageLimit(75)} class="p-3 items">75</div>
                        <div onClick={() => this.setPageLimit(50)} class="p-3 items">50</div>
                        <div onClick={() => this.setPageLimit(25)} class="p-3 items">25</div>
                        <div onClick={() => this.setPageLimit(10)} class="p-3 items mb-3">10</div>
                    </div>
                </div>
                <span className="ml-4 mr-4">{this.getMinByCurrent(this.props.page, this.state.limit)}-{this.getMaxByCurrent(this.props.page, this.props.limit)} of {this.props.total}</span>
                <img onClick={this.prevPage} className={`paging-icon p-cursor rorate-left mr-2 pt-3 ${ this.props.page === 1 ? 'opacity-3': ''}`} src="./images-asset/vector.png" />
                <img onClick={this.nextPage} className={`paging-icon p-cursor rorate-right pt-3 ${ this.props.page === this.props.pages ? 'opacity-3': ''}`} src="./images-asset/vector.png" />
            </div>
        )
    }
}

export default Pagination;




