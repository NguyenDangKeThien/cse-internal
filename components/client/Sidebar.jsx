import React, { Component } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import http from '../../plugins/http';
import AuthService from '../../services/auth.service';

// Redux
import * as actions from '../../redux/actions';
import { connect } from 'react-redux';
import * as menuIconSvg from '../../public/svgConfig';

class Sidebar extends Component {

    initalState = {
        router: this.props.router,
        user: this.props.user,
    }

    constructor(props) {
        super(props);

        this.state = this.initalState;
    }

    componentDidUpdate(prevProps) {
        if(prevProps.user !== this.props.user) {
            this.setState({
                user: this.props.user,
            })
        }
    }

    handleMenu = () => {
        this.props.setIsShow(true)
    }

    
    logOut = () => {
        AuthService.logOut();
        setTimeout(() => {
            Router.push({ pathname: '/login' });
            this.props.setUserData({});
        }, 500);
    }

    selectPage = (path) => {
        if(this.state.router.pathname === path)
            return this.props.setIsShow(false);
        // this.props.router.push({ pathname: path });
        Router.push(path, path, { shallow: true });
        this.setState({
            router: {
                pathname : path
            },
        })
        this.props.setIsShow(false);
    }

    getResponsiveTitle() {
        if (this.state.router.pathname === '/history') {
            return 'History'
        } else if (this.state.router.pathname === '/setting') {
            return 'Setting'
        } else {
            return 'CSE Wallet'
        }
    }

    // selectedItemSvg = () => {
    //     return (<svg className="select-item-svg" width="19" height="68" viewBox="0 0 19 68" fill="none" xmlns="http://www.w3.org/2000/svg">
    //         <path opacity="0.5" d="M-1.5391e-06 32.7894L19 -8.30517e-07L19 68L-1.5391e-06 32.7894Z" fill="#34AEC3"/>
    //     </svg>);
    // }

    render() {
        const { isMin } = this.props;
        return (
            <div key={ this.state.key }>
                <style jsx>{`
                    .bgr-avatar {
                        background: #50BFC3;
                        min-height: 183px;
                    }
                `}
                </style>
                <div className="gl-user-header d-none d-lg-flex">
                    <div className="text-white font-nomal text-capitalize text-16">
                        { this.state.user.username }
                    </div>
                    <div className="wrapper-avatar sidebar__avatar p-2">
                        {
                            !!Object.keys(this.state.user).length
                            ?
                            <div className="avatar mx-auto">
                                <img src={ this.state.user.avatarUrl ? this.state.user.avatarUrl : '/images-asset/No_Image_Available.jpg' } className="h-100 rounded-circle" alt="" />
                            </div>
                            :
                            <div className="avatar mx-auto"></div>
                        }
                    </div>
                    <div className="gl-user-header__dropdown">
                        <Link href={{ pathname: '/setting', query: { tab: 'personal' } }}>
                            <div className="py-3 d-flex items">
                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg" className="mr-2">
                                    <g opacity="0.5">
                                        <path d="M18.0896 15.8786C19.3286 14.1786 20 12.133 20 9.99954C20 4.47296 15.5275 0 10 0C4.47311 0 0 4.47235 0 9.99954C0 12.1324 0.671081 14.1774 1.90933 15.8771C5.89889 10.3894 14.0944 10.3845 18.0896 15.8786ZM6.48437 5.89813C6.48437 3.95981 8.06152 2.38266 10 2.38266C11.9385 2.38266 13.5156 3.95981 13.5156 5.89813V7.07001C13.5156 9.00848 11.9385 10.5855 10 10.5855C8.06152 10.5855 6.48437 9.00848 6.48437 7.07001V5.89813Z" fill="white"/>
                                        <path d="M10 9.41363C11.2924 9.41363 12.3437 8.3623 12.3437 7.07004V5.89816C12.3437 4.6059 11.2924 3.55457 10 3.55457C8.70758 3.55457 7.65625 4.6059 7.65625 5.89816V7.07004C7.65625 8.3623 8.70758 9.41363 10 9.41363Z" fill="white"/>
                                        <path d="M2.68555 16.8174C6.64215 21.0597 13.3562 21.0629 17.3164 16.8145C13.8252 11.6273 6.19553 11.651 2.68555 16.8174Z" fill="white"/>
                                    </g>
                                </svg>
                                Profile
                            </div>
                        </Link>
                        <Link href="#">
                            <div className="py-3 d-flex items" onClick={() => this.logOut()}>
                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg" className="mr-2">
                                    <g opacity="0.5">
                                        <path d="M19.9362 8.8481C19.8937 8.74642 19.8328 8.65392 19.7553 8.57642L17.2562 6.07725C16.9303 5.75225 16.4037 5.75225 16.0778 6.07725C15.752 6.40307 15.752 6.93056 16.0778 7.25556L17.1553 8.33306H12.5003C12.0395 8.33306 11.667 8.70638 11.667 9.16638C11.667 9.62638 12.0395 9.9997 12.5003 9.9997H17.1553L16.0778 11.0772C15.752 11.403 15.752 11.9305 16.0778 12.2555C16.2403 12.4188 16.4536 12.4997 16.667 12.4997C16.8803 12.4997 17.0937 12.4189 17.2562 12.2555L19.7553 9.75634C19.8328 9.67966 19.8937 9.58716 19.9362 9.48466C20.0203 9.28142 20.0203 9.05142 19.9362 8.8481Z" fill="white"/>
                                        <path d="M14.1666 11.6667C13.7058 11.6667 13.3333 12.04 13.3333 12.5V16.6666H9.99998V3.33331C9.99998 2.96581 9.7583 2.64082 9.4058 2.535L6.51163 1.66668H13.3333V5.83335C13.3333 6.29335 13.7058 6.66667 14.1666 6.66667C14.6274 6.66667 14.9999 6.29335 14.9999 5.83335V0.833358C14.9999 0.37332 14.6274 0 14.1666 0H0.833319C0.803319 0 0.776639 0.0125 0.747499 0.0158203C0.708319 0.02 0.672499 0.0266406 0.634999 0.0358202C0.547499 0.0583202 0.468319 0.0924998 0.394179 0.13914C0.375859 0.15082 0.353359 0.15164 0.335859 0.164961C0.32914 0.17 0.32664 0.179179 0.31996 0.184179C0.22914 0.25582 0.15332 0.34332 0.0983201 0.447499C0.0866405 0.469999 0.0841405 0.494179 0.0749999 0.517499C0.0483202 0.580819 0.0191797 0.642499 0.00917967 0.712499C0.00499999 0.737499 0.0125 0.760819 0.0116797 0.784999C0.0108594 0.801678 0 0.816678 0 0.833319V17.5C0 17.8975 0.28082 18.2391 0.669999 18.3166L9.0033 19.9833C9.05748 19.995 9.11248 20 9.16662 20C9.35744 20 9.54494 19.9342 9.69494 19.8108C9.88744 19.6525 9.99994 19.4166 9.99994 19.1666V18.3333H14.1666C14.6274 18.3333 14.9999 17.96 14.9999 17.5V12.5C14.9999 12.04 14.6274 11.6667 14.1666 11.6667Z" fill="white"/>
                                    </g>
                                </svg>
                                Log out
                            </div>
                        </Link>
                    </div>
                </div>
                <div className="d-lg-none d-block px-30 py-2 position-fixed w-100 bgr-dash" style={{ top: 0, zIndex: 120, padding : '36px'}}>
                    <div className="d-flex flex-row align-items-baseline justify-content-between py-2">
                        <div onClick={this.handleMenu}>
                            <svg width="30" height="20" viewBox="0 0 20 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <line x1="1" y1="1" x2="19" y2="1" stroke="#50BFC3" strokeWidth="2" strokeLinecap="round"/>
                                <line x1="1" y1="7.25" x2="19" y2="7.25" stroke="#50BFC3" strokeWidth="2" strokeLinecap="round"/>
                                <line x1="1" y1="13.5" x2="19" y2="13.5" stroke="#50BFC3" strokeWidth="2" strokeLinecap="round"/>
                            </svg>
                        </div>
                        <div className="text-white text-center pt-2">
                            <span className="cse-navbar-mobile-title text-16"> {this.getResponsiveTitle()} </span>
                        </div>
                        <div style={{ width: '30px' }}></div>
                    </div>
                </div>
                <div className={`w-100 sidebar ${isMin && 'sidebar-min'}`}>
                    <div className={`d-md-block menu ${this.props.isShow && 'active'} menu-left-mobile`}>
                        <div className={`position-fixed bgr-special cse-menu-left-wrap p-0 flex-col d-flex h-100vh text-white ${isMin ? 'col-lg-1' : 'col-lg-2'}`}>
                            <div className="bgr-avatar d-lg-none d-flex">
                                <div className="d-flex justify-content-center align-items-center flex-column w-100">
                                    <div className="wrapper-avatar sidebar__avatar p-2">
                                        {
                                            Object.keys(this.state.user).length
                                            ?
                                            <div className="avatar mx-auto">
                                                <img src={ this.state.user.avatarUrl ? this.state.user.avatarUrl : '/images-asset/No_Image_Available.jpg' } className="h-100 rounded-circle" alt="" />
                                            </div>
                                            :
                                            <div></div>
                                        }
                                    </div>
                                    <div className="py-2">
                                        <div className='text-center font-nomal text-capitalize text-16 user-name'>
                                            { this.state.user.username }
                                        </div>
                                        <span className="user-gmail">{this.state.user.email}</span>
                                    </div>
                                </div>
                            </div>
                            <Link href="/" >
                                <div className="px-4 pt-4 d-lg-flex d-none justify-content-center align-items-center cusor-pointer sidebar__logo">
                                    <svg width="60" height="51" viewBox="0 0 60 51" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M24.1553 14.4631L15.7529 0H43.3917L34.9894 14.4631H24.1553ZM33.5514 11.7597L38.9971 2.77096H20.2816L25.6614 11.7597H33.5514Z" fill="white"/>
                                        <path d="M15.8828 50.2963L24.2852 35.8333H35.1192L43.5216 50.2963H15.8828ZM38.9934 47.5254L33.6135 38.5366H25.7234L20.2777 47.5254H38.9934Z" fill="white"/>
                                        <path d="M36.3086 35.5486L42.5261 25.1414L36.3086 14.734L44.5066 0.74884L59.1965 25.1739L44.5063 49.6019L36.3086 35.5486ZM44.5796 44.4741L56.3128 25.1409L44.712 5.73955L39.3926 14.8021L45.2722 25.1744L39.3923 35.3472L44.5796 44.4741Z" fill="white"/>
                                        <path d="M0.196289 25.1739L14.8862 0.74884L23.0842 14.734L16.8667 25.1414L23.0842 35.5486L14.8864 49.6019L0.196289 25.1739ZM14.8132 44.4741L20.0004 35.3472L14.1206 25.1774L20.0002 14.8024L14.6807 5.73955L3.07998 25.1409L14.8132 44.4741Z" fill="white"/>
                                        <path d="M24.0191 34.6032L18.6016 25.1725L24.0205 16.0851H35.0532L40.746 25.1711L35.2587 34.6032H24.0191ZM33.6872 32.1026L37.9294 25.3414L33.7518 18.1802H25.1908L21.3455 25.1414L25.1908 32.1026H33.6872Z" fill="white"/>
                                    </svg>

                                    <div className="text-uppercase text-48 text-bold ml-3 sidebar__slogan">
                                        CSE
                                    </div>
                                </div>
                            </Link>
                            <div className="menu-items-container h-100 d-flex flex-column justify-content-between">
                                <div className="pt-lg-4 menu-items">
                                    <div onClick={() => this.selectPage('/') } className={`cusor-pointer d-flex d-lg-block sidebar__menu-item ${this.state.router.pathname == '/' ? 'sidebar__active-menu' : 'sidebar__inactive-menu'}`}>
                                        <div className="sidebar__menu px-xl-4 px-2">
                                            <div className="icon-menu">
                                                <div dangerouslySetInnerHTML={{__html: this.state.router.pathname === '/' ? menuIconSvg.menuDashBoardActive : menuIconSvg.menuDashBoard}} />
                                                {/*<img src={`./images-asset/menu-icon/menu-dashboard${this.state.router.pathname === '/' ? '-active': ''}.png`} className="h-100" alt="" />*/}
                                            </div>
                                            <div className="d-xl-block content pl-3 ml-2">
                                                Dashboard
                                            </div>
                                        </div>
                                    </div>
                                    <div onClick={() => this.selectPage('/send') } className={`cusor-pointer d-flex d-lg-block sidebar__menu-item ${this.state.router.pathname == '/send' ? 'sidebar__active-menu' : ''}`}>
                                        <div className="sidebar__menu px-xl-4 px-2">
                                            <div className="icon-menu">
                                                <div dangerouslySetInnerHTML={{__html: this.state.router.pathname === '/send' ? menuIconSvg.menuSendActive : menuIconSvg.menuSend}} />
                                                {/*<img src={`./images-asset/menu-icon/menu-send${this.state.router.pathname === '/send' ? '-active': ''}.png`} className="h-100" alt="" />*/}
                                            </div>
                                            <div className="d-xl-block content pl-3 ml-2">
                                                Send
                                            </div>
                                        </div>
                                    </div>
                                    <div onClick={() => this.selectPage('/receive') } className={`cusor-pointer d-flex d-lg-block sidebar__menu-item ${this.state.router.pathname == '/receive' ? 'sidebar__active-menu' : ''}`}>
                                        <div className="sidebar__menu px-xl-4 px-2">
                                            <div className="icon-menu">
                                                <div dangerouslySetInnerHTML={{__html: this.state.router.pathname === '/receive' ? menuIconSvg.menuReceiveActive : menuIconSvg.menuReceive}} />
                                                {/*<img src={`./images-asset/menu-icon/menu-receive${this.state.router.pathname === '/receive' ? '-active': ''}.png`} className="h-100" alt="" />*/}
                                            </div>
                                            <div className="d-xl-block content pl-3 ml-2">
                                                Receive
                                            </div>
                                        </div>
                                    </div>
                                    <div onClick={() => this.selectPage('/history') } className={`cusor-pointer d-flex d-lg-block sidebar__menu-item ${this.state.router.pathname == '/history' ? 'sidebar__active-menu' : ''}`}>
                                        <div className="sidebar__menu px-xl-4 px-2">
                                            <div className="icon-menu">
                                                <div dangerouslySetInnerHTML={{__html: this.state.router.pathname === '/history' ? menuIconSvg.menuTransactiondActive : menuIconSvg.menuTransaction}} />
                                                {/*<img src={`./images-asset/menu-icon/menu-history${this.state.router.pathname === '/history' ? '-active': ''}.png`} className="h-100" alt="" />*/}
                                            </div>
                                            <div className="d-xl-block content pl-3 ml-2">
                                                History
                                            </div>
                                        </div>
                                    </div>
                                    <div onClick={() => this.selectPage('/setting') } className={`cusor-pointer d-flex d-lg-block sidebar__menu-item ${this.state.router.pathname == '/setting' ? 'sidebar__active-menu' : ''}`}>
                                        <div className="sidebar__menu px-xl-4 px-2">
                                            <div className="icon-menu">
                                                <div dangerouslySetInnerHTML={{__html: this.state.router.pathname === '/setting' ? menuIconSvg.menuSettingActive : menuIconSvg.menuSetting}} />
                                                {/*<img src={`./images-asset/menu-icon/menu-setting${this.state.router.pathname === '/setting' ? '-active': ''}.png`} className="h-100" alt="" />*/}
                                            </div>
                                            <div className="d-xl-block content pl-3 ml-2">
                                                Setting
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="menu-items logout-icon">
                                    <Link href="#">
                                        <div className="cusor-pointer d-flex d-lg-block sidebar__menu-item ">
                                            <div className="sidebar__menu px-xl-4 px-2">
                                                <div className="icon-menu">
                                                    <svg onClick={() => this.logOut()} width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <circle cx="15" cy="15" r="14.5" transform="rotate(-180 15 15)" stroke="#FBBF23"/>
                                                        <path d="M9.16344 14.57L12.7344 10.6781C12.9523 10.4406 13.3057 10.4406 13.5235 10.6781C13.7415 10.9156 13.7415 11.3006 13.5235 11.5381L10.9051 14.3919L20.442 14.3919C20.7502 14.3919 21 14.6642 21 15C21 15.3358 20.7502 15.6081 20.442 15.6081L10.9051 15.6081L13.5234 18.4619C13.7414 18.6994 13.7414 19.0844 13.5234 19.3219C13.4145 19.4406 13.2717 19.5 13.1289 19.5C12.9861 19.5 12.8433 19.4406 12.7343 19.3219L9.16344 15.43C8.94552 15.1925 8.94552 14.8075 9.16344 14.57Z" fill="#FBBF23"/>
                                                    </svg>
                                                </div>
                                                <div className="d-xl-block content pl-3 ml-2">
                                                    Log out
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(null, actions)(Sidebar);