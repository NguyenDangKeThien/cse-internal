import React, { Component } from 'react';

class Heading extends Component {
    render() {
        const { heading, redirect } = this.props
        return (
            <div className="list-card align-items-center justify-content-between d-flex" >
                <div className="d-flex align-items-center">
                    <div className="img-hightlight">
                        <img src="./images-asset/hightlight.png" alt=""/>
                    </div>
                    <div>
                        <h3 className="title-dash m-0 text-24">{heading}</h3>
                    </div>
                </div>
                <div className="cusor-pointer cl-light-blue">
                    {redirect}
                </div>
            </div>
        )
    }
}

export default Heading
