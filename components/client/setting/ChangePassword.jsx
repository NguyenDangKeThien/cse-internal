import React, { Component } from 'react';
import { FormGroup, Input } from 'reactstrap';
import http from '../../../plugins/http';
import { toast } from 'react-toastify';
export default class ChangePasswordSetting extends Component {
    initialState = {
        currentPassword: '',
        newPassword: '',
        confirmPassword: '',
        formErrors: {
            currentPassword: '',
            newPassword: '',
            confirmPassword: '',
        },
        currentPasswordValid: false,
        newPasswordValid: false,
        confirmPasswordValid: false,
        formValid: false
    }
    constructor(props) {
        super(props);
        this.state = this.initialState;
    }

    handleUserInput (e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value}, () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let currentPasswordValid = this.state.currentPasswordValid;
        let newPasswordValid = this.state.newPasswordValid;
        let confirmPasswordValid = this.state.confirmPasswordValid;
        const isValidPassword = this.isValidPassword(value);

        if (fieldName === 'currentPassword') {
            if (!value) {
                currentPasswordValid = false;
                fieldValidationErrors.currentPassword = 'You must enter current password'
            } else {
                currentPasswordValid = isValidPassword;
                fieldValidationErrors.currentPassword = isValidPassword ? '' : 'Password must be 6 to 15 characters';
            }
        } else if (fieldName === 'newPassword') {
            if (!value) {
                newPasswordValid = false;
                fieldValidationErrors.newPassword = 'You must enter new password'
            } else {
                newPasswordValid = isValidPassword;
                fieldValidationErrors.newPassword = isValidPassword ? '' : 'Password must be 6 to 15 characters';

                confirmPasswordValid = this.state.confirmPassword ? ( value === this.state.confirmPassword) : true;
                fieldValidationErrors.confirmPassword =  confirmPasswordValid ? '' : 'Re-password incorrectly';
            }
        } else {
            if (!value) {
                confirmPasswordValid = false;
                fieldValidationErrors.confirmPassword = 'You must enter re-password'
            } else {
                confirmPasswordValid = this.state.newPassword ? ( value === this.state.newPassword) : true;
                fieldValidationErrors.confirmPassword =  confirmPasswordValid ? '' : 'Re-password incorrectly';
            }
        }

        this.setState({
            formErrors: fieldValidationErrors,
            currentPasswordValid,
            newPasswordValid,
            confirmPasswordValid
        }, this.validateForm);
    }

    validateForm() {
        this.setState({
            formValid: this.state.currentPasswordValid
                    && this.state.newPasswordValid
                    && this.state.confirmPassword
                    && this.state.confirmPasswordValid
        });
    }

    isValidPassword(value) {
        if (!value) return false;
        if (value.length < 6 || value.length > 15) return false;
        return true;
    }

    renderErrors = (field) => {
        return <div className="mt-2 text-13 text-left text-danger">{ this.state.formErrors[field] }</div>;
    }

    submitForm = (event) => {
        event.preventDefault();
        if (!this.state.formValid) {
            const { currentPassword, newPassword, confirmPassword } = this.state;
            const formFields = {
                currentPassword,
                newPassword,
                confirmPassword
            }
            for (const field in formFields) {
                this.validateField(field, formFields[field]);
            }
            return;
        }
        if (this.state.formValid) {
            const data = {
                oldPassword: this.state.currentPassword,
                newPassword: this.state.newPassword
            }
            http.put('auth/updatePasswordByUserId', data)
                .then((resp) => {
                    this.setState(this.initialState)
                    toast.success('Success! Your password has been changed');
                })
                .catch((resp) => {
                    this.setState({formValid: false});
                    if (resp && resp.data && resp.data.message === 'INCORRECT_PASSWORD') {
                        this.setState({
                            formErrors: {
                                ...this.state.formErrors,
                                currentPassword: 'Password is incorrect'
                            }
                        })
                    }
                });
        }
    }

    render() {
        return (
            <React.Fragment>
                <form onSubmit={(e) => this.submitForm(e)}>
                    <div className="px-50 h-100 justify-content-lg-between justify-content-center">
                        <div className="col-lg-6">
                            <div className="gl-setting-title">
                                Current password
                            </div>
                            <div className="input-icon password-input">
                                <Input className="gl-setting-input" type="password"
                                name="currentPassword"
                                value={this.state.currentPassword}
                                onChange={(event) => this.handleUserInput(event)}/>
                            </div>
                            {this.renderErrors('currentPassword')}
                            
                            <div className="gl-setting-title">
                                New password
                            </div>
                            <div className="input-icon password-input">
                                <Input className="gl-setting-input" type="password"
                                name="newPassword"
                                value={this.state.newPassword}
                                onChange={(event) => this.handleUserInput(event)}/>
                            </div>
                            {this.renderErrors('newPassword')}

                            <div className="gl-setting-title">
                                Confirm password
                            </div>
                            <div className="input-icon password-input">
                                <Input className="gl-setting-input" type="password"
                                name="confirmPassword"
                                value={this.state.confirmPassword}
                                onChange={(event) => this.handleUserInput(event)}/>
                            </div>
                            {this.renderErrors('confirmPassword')}

                            <div className="setting-submit-btn mt-3">
                                <button 
                                type="submit" 
                                className="btn btn-primary-custom font-weight-normal my-3 col-12">
                                    Change password
                                </button>
                            </div>
                        </div>
                    </div>
                </form>    
            </React.Fragment>
        )
    }
}
