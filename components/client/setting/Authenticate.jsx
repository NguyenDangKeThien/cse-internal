import React, { Component, Fragment } from 'react';
import SwitchComponent from '../Switch';
import http from '../../../plugins/http';
import { toast } from 'react-toastify';
import * as actions from '../../../redux/actions';
import { connect } from 'react-redux';

import Enable2FAPopup from '../popups/Enable2FAPopup';
import ConfirmationPopup from '../popups/ConfirmPopup';
import EnterCodePopup from '../popups/EnterCodePopup';

class AuthenticateSetting extends Component {
    constructor(props) {
        super(props);
        const {isHave2Fa} = this.props.user;
        this.state = {
            currentModal: '',
            qrCodeUrl: '',
            twoFACode: '',
            twoFACheckbox: isHave2Fa,
            emailCodeCheckbox: false,
        }
    }

    handleModal = (modalName = '') => {
        if (!modalName) {
            this.setState({currentModal: ''});
            return;
        }
        if (modalName === 'INPUT_RESET') {
            this.sendRequestReset2fa();
        }
        this.setState({currentModal: modalName});
    }

    sendRequestReset2fa = () => {
        http.post('/auth/requestReset2Fa', {email: this.props.user.email})
            .then(() => {
                // toast.success('Verification code has been sent to your email');
            })
            .catch(() => {
                toast.error('Failed! Server has errors');
            });
    }

    handle2faCheckboxChange = () => {
        const isHave2Fa = this.props.user.isHave2Fa;
        if (!isHave2Fa) {
            this.handleModal('ENABLE');
            this.generate2fa();
        } else {
            this.handleModal('DISABLE')
        }
    }

    handleCheckboxChange = () => {
        this.setState({
            emailCodeCheckbox: !this.state.emailCodeCheckbox
        })
        if (this.state.emailCodeCheckbox) {
            this.handleModal('DISABLE_EMAIL_CODE');
        }
    }

    reset2fa = (code) => {
        if (!code) { return; }
        const data = {
            email: this.props.user.email,
            codeVerify: code
        }
        http.get(`auth/confirmReset2Fa?email=${data.email}&codeVerify=${data.codeVerify}`)
            .then(async() => {
                const user = await http.get('auth/me');
                const userInfo = user.data.result && user.data.result.user;
                this.props.setUserData(userInfo);
                toast.success('Success! Two-factor Authentication has been reset');
                this.setState({
                    twoFACheckbox: false,
                });
                this.closeModal();
            }).catch((resp) => {
                this.setState({formValid :false});
                if (resp.data && resp.data.message ==='CODE_VERIFY_INVALID') {
                    this.props.setCodeInputFormError({
                        formErrors: {code: 'Verification code is incorrect'}
                    });
                } else {
                    this.setState({
                        twoFACheckbox: true,
                    });
                    this.closeModal();
                    toast.error('Failed! Server has errors');
                }
            });
    }

    disable2fa = (code) => {
        if (!code) { return; }
        http.delete(`auth/remove2fa?code2FA=${code}`)
            .then(async() => {
                const user = await http.get('auth/me');
                const userInfo = user.data.result && user.data.result.user;
                this.props.setUserData(userInfo);
                toast.success('Success! Two-factor Authentication has been disabled');
                this.setState({
                    twoFACheckbox: false,
                });
                this.closeModal();
            }).catch((resp) => {
                this.setState({formValid :false});
                if (resp.data && resp.data.message ==='AUTHENTICATION_2FA_FAILURE') {
                    this.props.setCodeInputFormError({
                        formErrors: {code: 'Authenticate code is incorrect'}
                    });
                } else {
                    this.setState({
                        twoFACheckbox: true,
                    });
                    this.closeModal();
                    toast.error('Failed! Server has errors');
                }
            });
    }

    create2fa = (code) => {
        if (!code) { return; }
        http.post('auth/confirm2Fa', {code2FA: code})
            .then(async() => {
                const user = await http.get('auth/me');
                const userInfo = user.data.result && user.data.result.user;
                this.props.setUserData(userInfo);
                toast.success('Success! Two-factor Authentication has been enabled');
                this.setState({
                    twoFACheckbox: true,
                });
                this.closeModal();
            })
            .catch((resp) => {
                this.setState({formValid :false});
                if (resp.data && resp.data.message ==='AUTHENTICATION_2FA_FAILURE') {
                    this.props.setCodeInputFormError({
                        formErrors: {code: 'Authenticate code is incorrect'}
                    });
                } else {
                    this.setState({
                        twoFACheckbox: false,
                    });
                    this.closeModal();
                    toast.error('Failed! Server has errors');
                }
            });
    }

    generate2fa = () => {
        this.resetQrCode();
        http.post('auth/generate2fa')
            .then((resp) => {
                if (resp.data.result && resp.data.result.qr && resp.data.result.secretKey) {
                    this.setState({
                        qrCodeUrl: resp.data.result.qr,
                        twoFACode: resp.data.result.secretKey,
                    });
                }
            })
            .catch(() => {
                toast.error('Failed! Server has errors');
            })
    }

    disableEmailCode = () => {
        //TODO
    }

    sendRequestEmailCode = () => {
        //TODO
    }

    resetQrCode = () => {
        this.setState({
            qrCodeUrl: '',
            twoFACode: '',
        });
    }

    closeModal = () => {
        this.handleModal();
        this.props.resetCodeInputFormError();
    }

    showReset2fa = () => {
        if (this.props.user.isHave2Fa) {
            return (
                <div className="col-lg-12 d-flex flex-wrap py-3">
                    You can also reset 2FA code via email verification code &nbsp;&nbsp;
                    <div className="cl-light-blue cusor-pointer" onClick={() => this.handleModal('RESET')}> Reset 2fa </div>
                </div>
            )
        } else {
            return ''
        }
    }

    showWarning = () => {
        if (!this.props.user.isHave2Fa) {
            return (
                <div className="warning">
                    <svg width="25" height="26" viewBox="0 0 25 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="12.1964" cy="12.1964" r="12.1964" fill="#FBBF23"/>
                        <path d="M12.124 6.48486C12.3949 6.48486 12.6058 6.56299 12.7568 6.71924C12.9131 6.87549 12.9912 7.0734 12.9912 7.31299C12.9912 7.54736 12.9131 7.74268 12.7568 7.89893C12.6058 8.04997 12.3949 8.12549 12.124 8.12549C11.8532 8.12549 11.6423 8.04997 11.4912 7.89893C11.3454 7.74268 11.2725 7.54736 11.2725 7.31299C11.2725 7.0734 11.3454 6.87549 11.4912 6.71924C11.6423 6.56299 11.8532 6.48486 12.124 6.48486ZM11.3818 18.0161V9.56299H12.8428V18.0161H11.3818Z" fill="white"/>
                    </svg>
                    &nbsp;&nbsp;
                    <p className="m-0 text-12-mobile"> Enable 2FA code before sending to protect your wallet </p>
                </div>
            )
        } else {
            return '';
        }
    }

    render() {
        return (
            <Fragment>
                <div className="px-55 h-100 account justify-content-lg-between justify-content-center">
                    <div className="col-lg-12">
                        <div className="text-24 text-16-mobile">
                            Two-factor Authentication
                        </div>
                        <p className="opacity-dot6 py-3 m-0"> Greatly increase security by requiring both your password and another form of authentication. </p>
                    </div>
                    <div className="col-lg-12">
                        <div className="account__controls d-flex justify-content-between align-items-center py-3">
                            <div className="control-description d-flex justify-content-center align-items-center">
                                <div className="control-icon">
                                    <div className="img-container">
                                        <img src="./images-asset/ggauthenticator.png"></img>
                                    </div>
                                </div>
                                <div className="control-name">
                                    <div>2FA code</div>
                                    <div className="opacity-dot6">Used for withdrawals</div>
                                </div>
                            </div>
                            <div className="control-switch">
                                <SwitchComponent 
                                name="twoFACheckbox"
                                checked={this.state.twoFACheckbox} 
                                onCheckboxChange={this.handle2faCheckboxChange}/>
                            </div>
                        </div>
                        <div className="account__controls d-flex justify-content-between align-items-center py-3">
                            <div className="control-description d-flex justify-content-center align-items-center">
                                <div className="control-icon">
                                    <svg width="58" height="43" viewBox="0 0 58 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M49.5607 2.51221L33.0293 19.0428L49.5602 35.5736C49.8343 35.0007 50.0006 34.3674 50.0006 33.6909V4.39423C50.0006 3.71816 49.8347 3.08496 49.5607 2.51221Z" fill="white"/>
                                        <path d="M45.6051 0H4.39442C3.71796 0 3.08466 0.166308 2.51172 0.440427L24.9021 22.8308L29.9205 18.0079C29.9205 18.0079 29.9209 18.0074 29.921 18.0072C29.9211 18.007 29.9215 18.0068 29.9215 18.0068L47.4885 0.440818C46.9155 0.166503 46.2818 0 45.6051 0Z" fill="white"/>
                                        <path d="M0.440427 2.51172C0.166308 3.08466 0 3.71796 0 4.39442V33.6911C0 34.3672 0.166112 35.0006 0.440036 35.5733L16.9715 19.0428L0.440427 2.51172Z" fill="white"/>
                                        <path d="M30.9576 21.1145L25.9385 25.938C25.6524 26.2241 25.2776 26.3672 24.9029 26.3672C24.5282 26.3672 24.1533 26.2241 23.8673 25.938L19.0435 21.1143L2.51172 37.6449C3.08476 37.9192 3.71845 38.0857 4.39511 38.0857H45.6058C46.2823 38.0857 46.9156 37.9194 47.4885 37.6453L30.9576 21.1145Z" fill="white"/>
                                        <g clipPath="url(#clip0)">
                                        <path d="M34 30.1964C34 24.0232 39.0228 19 45.195 19C51.3672 19 56.39 24.0232 56.39 30.1964C56.39 36.3695 51.3672 41.3927 45.195 41.3927C39.0228 41.3927 34 36.3695 34 30.1964Z" fill="white" stroke="#1D3752" strokeWidth="2"/>
                                        <path d="M51.3759 27.6117L44.7702 34.2179C44.572 34.4161 44.3119 34.5159 44.0517 34.5159C43.7916 34.5159 43.5314 34.4161 43.3333 34.2179L40.0305 30.9148C39.6331 30.5175 39.6331 29.875 40.0305 29.4777C40.4278 29.0802 41.07 29.0802 41.4674 29.4777L44.0517 32.0623L49.939 26.1746C50.3362 25.7771 50.9784 25.7771 51.3759 26.1746C51.7732 26.5719 51.7732 27.2142 51.3759 27.6117Z" fill="#B0B0B0"/>
                                        </g>
                                        <defs>
                                        <clipPath id="clip0">
                                        <rect x="33" y="18" width="24.39" height="24.3927" fill="white"/>
                                        </clipPath>
                                        </defs>
                                    </svg>
                                </div>
                                <div className="control-name">
                                    <div>Email code</div>
                                    <div className="opacity-dot6">Used for withdrawals</div>
                                </div>
                            </div>
                            <div className="control-switch">
                                <SwitchComponent 
                                name="emailCodeCheckbox"
                                checked={this.state.emailCodeCheckbox} 
                                onCheckboxChange={this.handleCheckboxChange}/>
                            </div>
                        </div>
                    </div>
                    {this.showReset2fa()}
                    <div className="col-lg-12">
                        <div className="account__footer pt-5">
                            <button type="button" className="btn btn-primary-custom w-100 font-weight-normal my-3" >
                                Save
                            </button>
                            {this.showWarning()}
                        </div>
                    </div>
                </div>

                <Fragment>
                    {/* Enable 2fa*/}
                    { this.state.currentModal === 'ENABLE' &&
                        <Enable2FAPopup
                        qrCodeUrl={this.state.qrCodeUrl}
                        twoFACode={this.state.twoFACode}
                        openPopup={this.handleModal}
                        closePopup={this.closeModal}>
                        </Enable2FAPopup> }

                    { this.state.currentModal === 'INPUT_ENABLE' &&
                        <EnterCodePopup
                        type={'ENABLE'}
                        submitCode={this.create2fa}
                        closePopup={this.closeModal}>
                        </EnterCodePopup> }

                    {/* Reset 2fa*/}
                    { this.state.currentModal === 'RESET' &&
                        <ConfirmationPopup
                        type={'RESET'}
                        openPopup={this.handleModal}
                        closePopup={this.closeModal}>
                        </ConfirmationPopup> }

                    { this.state.currentModal === 'INPUT_RESET' &&
                        <EnterCodePopup
                        type={'RESET'}
                        resendCode={this.sendRequestReset2fa}
                        submitCode={this.reset2fa}
                        closePopup={this.closeModal}>
                        </EnterCodePopup> }

                    {/* Disable 2fa*/}
                    { this.state.currentModal === 'DISABLE' &&
                        <ConfirmationPopup
                        type={'DISABLE'}
                        openPopup={this.handleModal}
                        closePopup={this.closeModal}>
                        </ConfirmationPopup> }

                    { this.state.currentModal === 'INPUT_DISABLE' &&
                        <EnterCodePopup
                        type={'DISABLE'}
                        submitCode={this.disable2fa}
                        closePopup={this.closeModal}>
                        </EnterCodePopup> }

                    {/* Disable email code*/}
                    { this.state.currentModal === 'DISABLE_EMAIL_CODE' &&
                        <ConfirmationPopup
                        type={'DISABLE_EMAIL_CODE'}
                        openPopup={this.handleModal}
                        closePopup={this.closeModal}>
                        </ConfirmationPopup> }

                    { this.state.currentModal === 'INPUT_DISABLE_EMAIL_CODE' &&
                        <EnterCodePopup
                        type={'DISABLE_EMAIL_CODE'}
                        resendCode={this.sendRequestEmailCode}
                        submitCode={this.disableEmailCode}
                        closePopup={this.closeModal}>
                        </EnterCodePopup> }

                </Fragment>
            </Fragment>
        )
    }
}
const mapDisPatchToProps = (dispatch, props) => {
    return {
        setUserData: (data) => {
            dispatch(actions.setUserData(data));
        },
        setCodeInputFormError: (data) => {
            dispatch(actions.actSetCodeInputFormError(data));
        },
        resetCodeInputFormError: () => {
            dispatch(actions.actResetCodeInputFormError());
        }
    }
};

const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

export default connect(mapStateToProps, mapDisPatchToProps)(AuthenticateSetting);