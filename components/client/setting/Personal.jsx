import React, { Component, Fragment } from 'react';
import { Input } from 'reactstrap';
import DatePicker from "react-datepicker";
import { storage } from "../../../firebase";
import http from '../../../plugins/http';
import { toast } from 'react-toastify';
// Redux
import * as actions from '../../../redux/actions';
import { connect } from 'react-redux';
class PersonalSetting extends Component {
    constructor (props) {
        super(props);
        const user = this.props.user;
        this.state = {
          fullName: user.fullName || '',
          email: user.email || '',
          birthDay: user.birthDay ? new Date(user.birthDay) : null,
          phoneNumber: user.phoneNumber || '',
          avatarUrl: user.avatarUrl || '',
          formErrors: {fullName: '', phoneNumber: '', birthDay: ''},
          fullNameValid: true,
          birthDayValid: true,
          phoneNumberValid: true,
          formValid: !true,
          isUploading: false
        }
    }

    componentDidMount() {
        this.validateForm();
    }

    handleDateChange = date => {
        this.setState({
            birthDay: date
        }, this.validateField('birthDay', date));
    };

    handleFileUpload= (e) => {
        this.setState({
            isUploading: true
        })
        if (e.target.files.length) {
            storage.uploadImage(e.target.files[0], (url) => {
                this.setState({
                    avatarUrl: url
                }, this.validateForm);
                this.setState({
                    isUploading: false
                })
            });
        } else {
            this.setState({
                    isUploading: false
            });
        }
    }

    handleUserInput (e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value}, () => { this.validateField(name, value) });
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let phoneNumberValid = this.state.phoneNumberValid;
        let fullNameValid = this.state.fullNameValid;
        let birthDayValid = this.state.birthDayValid;

        switch (fieldName) {
            case 'fullName':
                const fullName = value.normalize('NFC');
                fullNameValid = fullName.match(/^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/i);
                if (!value) {
                    fullNameValid = true;
                }
                fieldValidationErrors.fullName = fullNameValid ? '' : 'Special characters are not allowed';
                break;
            case 'birthDay':
                const today = new Date();
                const selectedDay = new Date(value);
                birthDayValid = selectedDay < today;
                if (!value) {
                    birthDayValid = true;
                }
                fieldValidationErrors.birthDay = birthDayValid ? '' : 'Birthday must not be future day';
                break;
            case 'phoneNumber':
                phoneNumberValid = value.match(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/i);
                if (!value) {
                    phoneNumberValid = true;
                }
                fieldValidationErrors.phoneNumber = phoneNumberValid ? '' : 'Phone number is incorrect';
                break;
            default:
                break;
        }
        this.setState({
            formErrors: fieldValidationErrors,
            fullNameValid,
            birthDayValid,
            phoneNumberValid
        }, this.validateForm);
    }

    validateForm() {
        const userSnapShot = {
            fullName: this.props.user.fullName,
            birthDay: this.props.user.birthDay,
            phoneNumber: this.props.user.phoneNumber,
            avatarUrl: this.props.user.avatarUrl
        };
        const currentUserData = {
            fullName: this.state.fullName,
            birthDay: this.state.birthDay,
            phoneNumber: this.state.phoneNumber,
            avatarUrl: this.state.avatarUrl
        }
        const isEqual = JSON.stringify(userSnapShot) === JSON.stringify(currentUserData)
        this.setState({formValid: this.state.fullNameValid && this.state.birthDayValid && this.state.phoneNumberValid && !isEqual});
    }

    renderErrors = (field) => {
        return <div className="mt-2 text-13 text-left text-danger">{ this.state.formErrors[field] }</div>;
    }

    submitForm = (event) => {
        event.preventDefault();
        if (this.state.formValid) {
            const data = {
                fullName: this.state.fullName,
                birthDay: this.state.birthDay,
                phoneNumber: this.state.phoneNumber,
            }
            http.put('auth/updateInformationByUserId', data)
                .then(async (resp) => {
                    // Update avatar
                    if (this.state.avatarUrl) {
                        await http.put('auth/updateAvatarByUserId', {avatarUrl: this.state.avatarUrl});
                    }
                    const user = await http.get('auth/me');
                    const userInfo = user.data.result && user.data.result.user;
                    this.props.setUserData(userInfo);
                    toast.success('Success! Your profile has been changed');
                    this.setState({formValid: false});
                })
                .catch(() => {
                    toast.error('Failed! Server has errors');
                    this.setState({formValid: false});
                });
        }
    }

    render() {
        return (
            <Fragment>
                <form onSubmit={(e) => this.submitForm(e)}>
                    <div className="px-50 h-100 justify-content-lg-between justify-content-center">
                        <div className="d-flex flex-wrap">
                            <div className="col-lg-12 text-center-mobile">
                                <label htmlFor="file" className="profile-info-avatar cusor-pointer mb-3 col">
                                    <input type="file" className="d-none" name="file" id="file" accept="image/*" onChange={this.handleFileUpload}/>
                                    <div className="circle">
                                        {this.state.isUploading ? (<i className="fas fa-spinner"></i>) : '' }
                                        {this.state.avatarUrl ? (<img className="w-100 h-100" src={this.state.avatarUrl} ></img>) : '' }
                                    </div>
                                    <svg className="edit-icon" width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <ellipse cx="14.6734" cy="14.8285" rx="10.3076" ry="10.3638" fill="white"/>
                                        <path d="M 15.079 0.505333 C 7.31204 0.505333 1.01555 6.83663 1.01555 14.6456 C 1.01555 22.4546 7.31204 28.7865 15.079 28.7865 C 22.8458 28.7865 29.1431 22.455 29.1431 14.6456 C 29.1431 6.83626 22.8458 0.505333 15.079 0.505333 Z M 21.7645 10.6814 L 20.4288 12.0244 L 17.7096 9.29068 L 16.6781 10.3278 L 19.3972 13.0618 L 12.7279 19.7669 L 10.009 17.0334 L 8.9775 18.0706 L 11.6964 20.8043 L 11.0319 21.4725 L 11.0192 21.4598 C 10.9455 21.5818 10.8256 21.6708 10.6834 21.7025 L 8.1478 22.271 C 8.10983 22.2795 8.07119 22.2837 8.03303 22.2837 C 7.89465 22.2837 7.76022 22.2289 7.66054 22.1285 C 7.53266 22.0003 7.47942 21.8153 7.51875 21.6379 L 8.08386 19.0893 C 8.11571 18.9464 8.20444 18.8256 8.32558 18.7516 L 8.31271 18.7388 L 19.0453 7.94713 C 19.2026 7.78926 19.4583 7.78926 19.6156 7.94762 L 21.7647 10.108 C 21.9221 10.2662 21.9221 10.5232 21.7645 10.6814 Z" fill="#34AEC3"/>
                                    </svg>
                                </label>
                            </div>
                            <div className="col-lg-6">
                                <div className="gl-setting-title">
                                    User name
                                </div>
                                <Input
                                className="gl-setting-input pl-2"
                                type="text"
                                name="fullName"
                                value={this.state.fullName}
                                onChange={(event) => this.handleUserInput(event)}/>
                                {this.renderErrors('fullName')}
                            </div>
                            <div className="col-lg-6">
                                <div className="gl-setting-title">
                                    Birthday
                                </div>
                                <DatePicker
                                    className="w-100 gl-setting-input pl-2"
                                    isClearable
                                    selected={this.state.birthDay}
                                    onChange={this.handleDateChange}
                                />
                                {this.renderErrors('birthDay')}
                            </div>
                            <div className="col-lg-6">
                                <div className="gl-setting-title">
                                    Email address
                                </div>
                                <Input
                                className="gl-setting-input pl-2"
                                type="text"
                                name="email"
                                readOnly={true}
                                value={this.state.email}
                                onChange={(event) => this.handleUserInput(event)}/>
                            </div>
                            <div className="col-lg-6">
                                <div className="gl-setting-title">
                                    Phone number
                                </div>
                                <Input
                                className="gl-setting-input pl-2"
                                type="text"
                                name="phoneNumber"
                                value={this.state.phoneNumber}
                                onChange={(event) => this.handleUserInput(event)}/>
                                {this.renderErrors('phoneNumber')}
                            </div>
                        </div>
                        <div className="setting-submit-btn col-lg-6 mt-3">
                            <button
                            type="submit"
                            className="btn btn-primary-custom w-100 my-3 font-weight-normal col-12">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </Fragment>
        )
    }
}
const mapDisPatchToProps = (dispatch, props) => {
    return {
        setUserData: (data) => {
            dispatch(actions.setUserData(data));
        }
    }
};

const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

export default connect(mapStateToProps, mapDisPatchToProps)(PersonalSetting);