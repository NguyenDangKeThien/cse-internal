import React, { Component, Fragment } from 'react';
import SwitchComponent from '../Switch'

export default class SecuritySetting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            authEmailCheckbox: false,
            twoFACheckbox: false,
            emailCodeCheckbox: false,
        }
    }

    handleCheckboxChange = (name, value) => {
        this.setState({ [name]: value });
    }

    render() {
        return (
            <Fragment>
                <div className="px-55 h-100 account justify-content-lg-between justify-content-center">
                    <div className="col-lg-12">
                        <div className="d-flex justify-content-between align-items-center">
                            <div className="text-24 text-16-mobile">
                                Withdraw
                            </div>
                            <div className="text-center">
                                <SwitchComponent 
                                name="authEmailCheckbox"
                                checked={this.state.authEmailCheckbox} 
                                onCheckboxChange={this.handleCheckboxChange}/>
                            </div>
                        </div>
                        <p className="opacity-dot6 pt-2 m-0"> Authentication with email </p>
                        <div className="py-4" style={{ borderBottom:  '1px solid #50BFC3'}}></div>
                    </div>
                    <div className="col-lg-12">
                        <div className="text-24 text-16-mobile py-3">
                            Sign in
                        </div>
                        <div className="account__controls d-flex justify-content-between align-items-center py-3">
                            <div className="control-description d-flex justify-content-center align-items-center">
                                <div className="control-icon">
                                    <div className="img-container">
                                        <img src="./images-asset/ggauthenticator.png"></img>
                                    </div>
                                </div>
                                <div className="control-name">
                                    <div>2FA code</div>
                                    <div className="opacity-dot6">Used for sign-in</div>
                                </div>
                            </div>
                            <div className="control-switch">
                                <SwitchComponent 
                                name="twoFACheckbox"
                                checked={this.state.twoFACheckbox} 
                                onCheckboxChange={this.handleCheckboxChange}/>
                            </div>
                        </div>
                        <div className="account__controls d-flex justify-content-between align-items-center py-3">
                            <div className="control-description d-flex justify-content-center align-items-center">
                                <div className="control-icon">
                                    <svg width="58" height="43" viewBox="0 0 58 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M49.5607 2.51221L33.0293 19.0428L49.5602 35.5736C49.8343 35.0007 50.0006 34.3674 50.0006 33.6909V4.39423C50.0006 3.71816 49.8347 3.08496 49.5607 2.51221Z" fill="white"/>
                                        <path d="M45.6051 0H4.39442C3.71796 0 3.08466 0.166308 2.51172 0.440427L24.9021 22.8308L29.9205 18.0079C29.9205 18.0079 29.9209 18.0074 29.921 18.0072C29.9211 18.007 29.9215 18.0068 29.9215 18.0068L47.4885 0.440818C46.9155 0.166503 46.2818 0 45.6051 0Z" fill="white"/>
                                        <path d="M0.440427 2.51172C0.166308 3.08466 0 3.71796 0 4.39442V33.6911C0 34.3672 0.166112 35.0006 0.440036 35.5733L16.9715 19.0428L0.440427 2.51172Z" fill="white"/>
                                        <path d="M30.9576 21.1145L25.9385 25.938C25.6524 26.2241 25.2776 26.3672 24.9029 26.3672C24.5282 26.3672 24.1533 26.2241 23.8673 25.938L19.0435 21.1143L2.51172 37.6449C3.08476 37.9192 3.71845 38.0857 4.39511 38.0857H45.6058C46.2823 38.0857 46.9156 37.9194 47.4885 37.6453L30.9576 21.1145Z" fill="white"/>
                                        <g clipPath="url(#clip0)">
                                        <path d="M34 30.1964C34 24.0232 39.0228 19 45.195 19C51.3672 19 56.39 24.0232 56.39 30.1964C56.39 36.3695 51.3672 41.3927 45.195 41.3927C39.0228 41.3927 34 36.3695 34 30.1964Z" fill="white" stroke="#1D3752" strokeWidth="2"/>
                                        <path d="M51.3759 27.6117L44.7702 34.2179C44.572 34.4161 44.3119 34.5159 44.0517 34.5159C43.7916 34.5159 43.5314 34.4161 43.3333 34.2179L40.0305 30.9148C39.6331 30.5175 39.6331 29.875 40.0305 29.4777C40.4278 29.0802 41.07 29.0802 41.4674 29.4777L44.0517 32.0623L49.939 26.1746C50.3362 25.7771 50.9784 25.7771 51.3759 26.1746C51.7732 26.5719 51.7732 27.2142 51.3759 27.6117Z" fill="#B0B0B0"/>
                                        </g>
                                        <defs>
                                        <clipPath id="clip0">
                                        <rect x="33" y="18" width="24.39" height="24.3927" fill="white"/>
                                        </clipPath>
                                        </defs>
                                    </svg>
                                </div>
                                <div className="control-name">
                                    <div>Email code</div>
                                    <div className="opacity-dot6">Used for sign-in</div>
                                </div>
                            </div>
                            <div className="control-switch">
                                <SwitchComponent 
                                name="emailCodeCheckbox"
                                checked={this.state.emailCodeCheckbox} 
                                onCheckboxChange={this.handleCheckboxChange}/>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-12 d-flex flex-wrap py-3">
                        You can also reset 2FA code via email verification code &nbsp;&nbsp;
                        <div className="cl-light-blue cusor-pointer"> Reset 2fa </div>
                    </div>
                    <div className="setting-submit-btn pt-5 col-lg-12">
                        <button type="button" className="btn btn-primary-custom font-weight-normal my-3 col-12" >
                            Save
                        </button>
                    </div>
                </div>
            </Fragment>
        )
    }
}
