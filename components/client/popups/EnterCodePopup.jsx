import React, { Component, Fragment } from 'react';
import * as actions from '../../../redux/actions';
import { connect } from 'react-redux';
import OtpInput from '../../../libs/otp-input';
import {
    Modal,
    ModalHeader,
    ModalBody,
    Input
} from "reactstrap";

class EnterCodePopup extends Component {
    constructor(props) {
        super(props);
        this.codeSnapShot = '';
        this.errorMessageSnapShot = '';
        this.state = {
            isOpen: true,
            code: [],
            numInputs: 6,
            type: this.props.type
        }
    }

    handleOtpChange = value => {
        this.setState({code: value}, this.validateCode(value));
    };

    validateCode(value) {
        const code = value && value.length && value.join('');
        if (this.props.formErrors && this.props.formErrors.code) {
            this.errorMessageSnapShot = this.props.formErrors.code;
            this.props.setCodeInputFormError({
                formErrors: {code: ''}
            });
        }
        let codeValid = !!this.props.formErrors.code;
        codeValid = code.length === 6;
        if (codeValid) {
            if (this.codeSnapShot !== code) {
                this.codeSnapShot = code;
                this.props.submitCode(code);
            } else {
                this.props.setCodeInputFormError({
                    formErrors: {code: this.errorMessageSnapShot}
                });
            }
        }
    }

    renderErrors = (field) => {
        return <div className="pt-3 text-13 text-center text-danger">{ this.props.formErrors[field] }</div>;
    }


    getTitleHeader = () => {
        if (this.state.type === 'RESET') {
            return 'Reset 2FA code'
        } else if (this.state.type === 'DISABLE') {
            return 'Disable 2FA code'
        } else if (this.state.type === 'ENABLE') {
            return 'Authentication code'
        } else {
            return 'Email code'
        }
    }

    getErrorCode = () => {
        if (this.state.type === 'RESET' || this.state.type === 'DISABLE_EMAIL_CODE') {
            return 'Verification code is invalid';
        } else {
            return 'Authentication code is invalid';
        }
    }

    getInputTitle = () => {
        if (this.state.type === 'RESET' || this.state.type === 'DISABLE_EMAIL_CODE') {
            return 'Enter 6 digit code sent to your email';
        } else {
            return 'Enter 6 digit code in your authenticator';
        }
    }

    resendCode = () => {
        this.props.resendCode();
    }

    showResendMessage = () => {
        if (this.state.type === 'RESET' || this.state.type === 'DISABLE_EMAIL_CODE') {
            return (
                <div className="text-center pt-5">
                    <span className="cl-white opacity-dot6 font-weight-light">
                        Not received email?
                    </span>
                    <span 
                    className="cl-blue font-weight-bold cusor-pointer"
                    onClick={this.resendCode}>
                        &nbsp;Resend
                    </span>
                </div>
            )
        } else {
            return (
                <div className="pt-5 text-center opacity-dot6 cusor-pointer">
                    Can’t access to Google Authenticator?
                </div>
            )
        }
    }

    close = () => {
        this.setState({isOpen: false});
        this.props.closePopup();
    }

    submitCode = () => {
        this.props.submitCode(this.state.code);
    }

    render() {
        return (
            <Fragment>
                <Modal isOpen={this.state.isOpen} toggle={this.close} >
                    <ModalHeader className="mx-auto border-none text-24">
                    {this.getTitleHeader()}
                    <div className="close-modal cusor-pointer" onClick={this.close}>
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M20 2.02145L17.9786 0L9.99997 7.97858L2.02145 0L0 2.02145L7.97858 9.99997L0 17.9786L2.02145 20L9.99997 12.0214L17.9786 20L20 17.9786L12.0214 9.99997L20 2.02145Z" fill="#50BFC3"/>
                        </svg>
                    </div>
                    </ModalHeader>
                    <div className="w-100" style={{ borderBottom: '1px solid #50BFC3' }}></div>
                    <ModalBody>
                        <Fragment>
                            <div className="mr-lg-2 pt-3 text-center">
                                {this.getInputTitle()}
                            </div>
                            <div className="pt-4 code-input">
                                <OtpInput
                                inputStyle='form-control'
                                numInputs={this.state.numInputs}
                                onChange={this.handleOtpChange}
                                shouldAutoFocus={true}
                                value={this.state.code}
                                />
                            </div>
                            {this.renderErrors('code')}
                            {this.showResendMessage()}
                        </Fragment>
                    </ModalBody>
                </Modal>
            </Fragment>
        )
    }
}

const mapDisPatchToProps = (dispatch, props) => {
    return {
        setCodeInputFormError: (data) => {
            dispatch(actions.actSetCodeInputFormError(data));
        }
    }
};

const mapStateToProps = (state) => {
    return {
        formErrors: state.formValidate && state.formValidate.formInputCode && state.formValidate.formInputCode.formErrors,
    };
};

export default connect(mapStateToProps, mapDisPatchToProps)(EnterCodePopup);