import React, { Component, Fragment } from 'react';

import {
    Modal,
    ModalHeader,
    ModalBody,
} from "reactstrap";

class ConfirmationPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true,
            type: 'this.props.type'
        }
    }

    static getDerivedStateFromProps(props, state) {
        return {
            ...state,
            type: props.type
        };
    }

    openPopup = () => {
        if (this.state.type === 'RESET') {
            this.props.openPopup('INPUT_RESET');
        } else if (this.state.type === 'DISABLE') {
            this.props.openPopup('INPUT_DISABLE');
        } else {
            this.props.openPopup('INPUT_DISABLE_EMAIL_CODE');
        }
    }

    close = () => {
        this.setState({isOpen: false});
        this.props.closePopup();
    }

    getTitleHeader = () => {
        if (this.state.type === 'RESET') {
            return 'Reset 2FA code'
        } else if (this.state.type === 'DISABLE') {
            return 'Disable 2FA code'
        } else {
            return 'Disable email code'
        }
    }

    getBody = () => {
        if (this.state.type === 'RESET') {
            return 'Are you sure to reset 2fa code?'
        } else if (this.state.type === 'DISABLE') {
            return 'Are you sure to disable 2fa code?'
        } else {
            return 'Are you sure to disable email code?'
        }
    }

    render() {
        return (
            <Fragment>
                <Modal isOpen={this.state.isOpen} toggle={this.close} >
                    <ModalHeader className="mx-auto border-none text-24">
                        {this.getTitleHeader()}
                    <div className="close-modal cusor-pointer" onClick={this.close}>
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M20 2.02145L17.9786 0L9.99997 7.97858L2.02145 0L0 2.02145L7.97858 9.99997L0 17.9786L2.02145 20L9.99997 12.0214L17.9786 20L20 17.9786L12.0214 9.99997L20 2.02145Z" fill="#50BFC3"/>
                        </svg>
                    </div>
                    </ModalHeader>
                    <div className="w-100" style={{ borderBottom: '1px solid #50BFC3' }}></div>
                    <ModalBody className="text-center">
                        <Fragment>
                            <p className="mt-4">{this.getBody()}  </p>
                            <div className="d-flex justify-content-center py-4">
                                <button className="btn btn-primary-custom mr-4 mw-p-120" onClick={this.close}> No </button>
                                <button className="btn btn-secondary-custom mw-p-120" onClick={this.openPopup}> Yes </button>
                            </div>
                        </Fragment>
                    </ModalBody>
                </Modal>
            </Fragment>
        )
    }
}

export default ConfirmationPopup;