import React, { Component, Fragment } from 'react';

import {
    Modal,
    ModalHeader,
    ModalBody,
} from "reactstrap";

class Enable2FAPopup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: true,
            qrCodeUrl: '',
            twoFACode: '',
            isTextCopied: false,
            agreementCheckbox: false,
            isShowImage: false,
            formErrors: {
                agreement: ''
            }
        }
    }

    static getDerivedStateFromProps(props, state) {
        return {
            ...state,
            qrCodeUrl: props.qrCodeUrl,
            twoFACode: props.twoFACode
        };
    }

    copyText = () => {
        const input = document.createElement('textarea');
        input.innerHTML = this.state.twoFACode;
        document.body.appendChild(input);
        input.select();
        input.setSelectionRange(0, 99999); /*For mobile devices*/
        document.execCommand('copy');
        document.body.removeChild(input);
        this.setState({
            isTextCopied: true
        });
        setTimeout(() => {
            this.setState({
                isTextCopied: false
            });
        }, 3000);
    }

    handleImageLoaded = () => {
        this.setState({isShowImage: true});
    }

    handleAgreementCheckboxChange = (e) => {
        this.setState({ 
            formErrors: { agreement: '' },
            agreementCheckbox: e.target.checked
        });
    }

    openPopup = () => {
        if (!this.state.agreementCheckbox) {
            this.setState({
                formErrors: { agreement: 'You must accept the agreement' }
            })
            return;
        }
        this.props.openPopup('INPUT_ENABLE');
    }

    close = () => {
        this.setState({isOpen: false});
        this.props.closePopup();
    }

    render() {
        return (
            <Fragment>
                <Modal isOpen={this.state.isOpen} toggle={this.close} >
                    <ModalHeader className="mx-auto border-none text-24">
                    Enable 2FA code
                    <div className="close-modal cusor-pointer" onClick={this.close}>
                        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M20 2.02145L17.9786 0L9.99997 7.97858L2.02145 0L0 2.02145L7.97858 9.99997L0 17.9786L2.02145 20L9.99997 12.0214L17.9786 20L20 17.9786L12.0214 9.99997L20 2.02145Z" fill="#50BFC3"/>
                        </svg>
                    </div>
                    </ModalHeader>
                    <div className="w-100" style={{ borderBottom: '1px solid #50BFC3' }}></div>
                    <ModalBody>
                        <Fragment>
                            <div className="p-2">
                                <div className="mx-auto gl-setting-qrcode">
                                    <div className={`account__waiting-popup-content ${this.state.isShowImage ? 'd-none' : 'd-flex'}`}>
                                        <i className='fas fa-spinner'></i>
                                    </div>
                                    <div className={`account__qr-code-container ${this.state.isShowImage ? 'd-flex' : 'd-none'}`}>
                                        <img src={this.state.qrCodeUrl} alt="" onLoad={this.handleImageLoaded}/>
                                    </div>
                                </div>
                                <div className="pt-4 d-lg-flex">
                                    <div className="mr-lg-2 pt-3" style={{ opacity: .6 }}>
                                        Security code
                                    </div>
                                    <div className="d-flex align-items-center gl-setting-input bdr-10 p-2 col my-2">
                                        <div className="col px-0 mr-2 gl-hidden-x"> {this.state.twoFACode}</div>
                                        <div className="cusor-pointer" style={{width: '20px'}} onClick={this.copyText}>
                                            <img className="not-display-mobile" src="./images-asset/copy-item.png" alt=""></img>
                                            <img className="d-md-none" src="./images-asset/save.png" alt=""></img>
                                        </div>
                                    </div>
                                </div>
                                <p className="text-copy-success text-right">{this.state.isTextCopied ? 'Security code has been copied !' : ''}</p>
                                <div className="account__popup-footer d-flex justify-content-between align-items-center">
                                    <div className="d-flex flex-column">
                                        <div className="agreement d-flex justify-content-between align-items-center mt-4">
                                            <div className="checkbox-round">
                                                <input type="checkbox" id="checkbox" value={this.state.agreementCheckbox} onChange={this.handleAgreementCheckboxChange}/>
                                                <label className="m-0" htmlFor="checkbox"></label>
                                            </div>
                                            &nbsp;&nbsp;I have written down this backup on paper
                                        </div>
                                        <div className="mt-2 text-13 text-left text-danger">{ this.state.formErrors.agreement }</div>
                                    </div>
                                    <button type="button" className="btn btn-primary-custom font-weight-normal mw-p-150 w-20-mobile mt-4"
                                    onClick={this.openPopup}>
                                        OK
                                    </button>
                                </div>
                            </div>
                        </Fragment>
                    </ModalBody>
                </Modal>
            </Fragment>
        )
    }
}

export default Enable2FAPopup;