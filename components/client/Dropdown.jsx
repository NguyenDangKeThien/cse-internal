import React, { Component } from 'react'

class Dropdown extends Component {

    state = {
        nameCoin: 'CSE',
        isDrop: false,
    }

    componentDidUpdate() {
        const { isDrop } = this.state
        setTimeout(() => {
            if (isDrop) {
                window.addEventListener('click', this.close)
            }
            else {
                window.removeEventListener('click', this.close)
            }
        }, 0)
    }

    componentWillUnmount() {
        window.removeEventListener('click', this.close)
    }

    close = () => {
        this.setState({
            isDrop: false
        })
    }

    toggleDrop = () => {
        this.setState(prevState => ({
            isDrop: !prevState.isDrop
        }))
    }

    changeCurrency = (codeCoin) => {
        this.setState({
            nameCoin: codeCoin
        })
    }

    render() {
        const { nameCoin, isDrop } = this.state
        const {dropName} = this.props
        return (
            <div className="col-md-9 col-12 p-lg-0">
                <div onClick={() => this.toggleDrop()} className="d-flex align-items-center justify-content-between bdr-10 px-3 py-2 cusor-pointer form-dropdown">
                    <div className="d-flex align-items-center">
                        {
                            dropName === 'currency' && (
                                <div className="cusor-pointer d-flex align-items-center"> 
                                    <img src="./images-asset/cse-text-icon.png" alt="" />
                                    <div className="ml-2">{nameCoin}</div>
                                </div>
                            )
                        }
                        {
                            dropName === 'wallet' && (
                                <div className="bdr-10">
                                    CSE Wallet {/*<span className="cl-blue opacity-dot6">(20.544CSE)</span>*/}
                                </div>
                            )
                        }

                    </div>
                   {/* <div>
                        <div className="cusor-pointer">
                            <img src="./images-asset/dropdown.png" alt="" />
                        </div>
                    </div>*/}
                    {/*<div className="gl-cus-dropdown__content">
                        <div className="p-3 items">
                            1 Day
                        </div>
                        <div className="p-3 items">
                            1 Week
                        </div>
                        <div className="p-3 items">
                            1 Month
                        </div>
                    </div>*/}
                </div>
                <div className={`bdr-10 bgr-white text-dark drop-selector py-2 ${isDrop && 'active'}`}>
                    <div className="d-flex align-items-center px-3 py-2 cusor-pointer">
                        <div className="cusor-pointer"> <img src="./images-asset/cse.png" alt="" /> </div>
                        <div className="px-2 selector-coin">CSE </div>
                    </div>
                    <div className="d-flex align-items-center px-3 py-2 cusor-pointer" onClick={() => this.changeCurrency('Bitcoin')}>
                        <div className="cusor-pointer"> <img src="./images-asset/bitcoin.png" alt="" /> </div>
                        <div className="px-2 selector-coin">Bitcoin </div>
                    </div>
                    <div className="d-flex align-items-center px-3 py-2 cusor-pointer" onClick={() => this.changeCurrency('Ether')}>
                        <div className="cusor-pointer"> <img src="./images-asset/ether.png" alt="" /> </div>
                        <div className="px-2 selector-coin">Ether </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Dropdown;
