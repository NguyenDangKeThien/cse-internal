import React, { Component } from 'react';
import propTypes from 'prop-types'

const InitTitle = ({ title, subTitle, classBonus }) =>
(
    <React.Fragment>
        <div className={classBonus}>
            <h3 className='m-0 text-uppercase f-fml font-weight-normal font-bebas text-54'> {title}</h3>
            <p className="font-weight-light">{subTitle} </p>
        </div>
    </React.Fragment>
)
InitTitle.propTypes = ({
    title: propTypes.string,
    subTitle: propTypes.string,
    classBonus: propTypes.string,
})
export default InitTitle;