import React, { Component, Fragment } from 'react';
import http from '../../plugins/http';
import SwiperCards from './SwiperCards.jsx'
import { connect }            from 'react-redux';
import { bindActionCreators } from "redux";
import { selectWallet, fetchWalletData } from '../../redux/actions/wallet-actions';

class Wallet extends Component {
    componentDidMount(){
        this.props.fetchWalletData();   
    }
    render() {
        return (
            <div>
                <SwiperCards currentSlide={1} selectSlide={this.props.selectWallet} walletsInfo={this.props.walletReducer}></SwiperCards>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    walletReducer: state.walletReducer,
});

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    { selectWallet, fetchWalletData },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Wallet);
