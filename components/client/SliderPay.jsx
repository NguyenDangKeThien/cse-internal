import React, { Component } from 'react';

const ONE = 'ONE';
const TWO = 'TWO';
const THREE = 'THREE';

export default class SliderPay extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isActive: TWO
        }
    }
    handleSlider = (choose) => {
        this.setState({
            isActive: choose
        })
    }
    render() {
        const { isActive } = this.state
        return (
            <div className="slider cusor-pointer">
                <div className="slider__card d-flex align-items-center justify-contents-between">
                    {
                        isActive !== THREE && (
                            <div className={`col-lg-4 col-12 ${isActive === ONE && 'offset-4'}`} onClick={() => this.handleSlider(ONE)}>
                                <div className={`card-pay w-100 h-100 position-relative ${isActive === ONE && 'active'}`}>
                                    <img src="./images-asset/eth-active.png" alt="" />
                                    <div className="card-content position-absolute">
                                        <p className="m-0">$5438.31</p>
                                        <span>20.544 ETH</span>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                    <div className="col-lg-4 col-12" onClick={() => this.handleSlider(TWO)}>
                        <div className={`card-pay w-100 h-100 position-relative ${isActive === TWO && 'active'}`}>
                            <img src="./images-asset/btc.png" alt="" />
                            <div className="card-content position-absolute">
                                <p className="m-0">$5438.31</p>
                                <span>20.544 ETH</span>
                            </div>
                        </div>
                    </div>
                    {
                        isActive !== ONE && (
                            <div className="col-lg-4 col-12" onClick={() => this.handleSlider(THREE)}>
                                <div className={`card-pay w-100 h-100 position-relative ${isActive === THREE && 'active'}`}>
                                    <img src="./images-asset/eth-active.png" alt="" />
                                    <div className="card-content position-absolute">
                                        <p className="m-0">$5438.31</p>
                                        <span>20.544 ETH</span>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                </div>
            </div >
        )
    }
}
