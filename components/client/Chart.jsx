import React, { Component } from 'react';
import { Line } from 'react-chartjs-2';
import WindowSizeListener from 'react-window-size-listener';
const BACKGROUND_COLOR = 'rgba(45, 156, 219, 0.09)';
const BORDER_COLOR = '#50BFC3';

class ChartComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                labels: ['', '15h', '16h', '17h', '18h', '19h', '20h', '21h', '22h'],
                datasets: [
                    {
                        label: '',
                        cubicInterpolationMode: 'monotone',
                        fill: true,
                        lineTension: 0.1,
                        pointLabelFontColor: '#fff',
                        backgroundColor: BACKGROUND_COLOR,
                        borderColor: BORDER_COLOR,
                        borderCapStyle: 'square',
                        borderDash: [],
                        borderWidth: 2,
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'round',
                        pointBackgroundColor: '#3FC2BB',
                        pointBorderWidth: 1,
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 20,
                        pointRadius: 5,
                        pointHitRadius: 5,
                        minBarLength: 200,
                        data: [
                            153,
                            106,
                            105,
                            153,
                            181,
                            45,
                            327,
                            181,
                            105,
                            327,
                        ]
                    },
                ]
            }
        }
    }

    onResize = (windowSize) => {
        const newState = this.state;
        if (windowSize.windowWidth <= 768) {
            newState.data.datasets[0].pointHoverBorderWidth = 8;
            newState.data.datasets[0].pointRadius = 2;
            newState.data.datasets[0].pointHitRadius = 2;
            newState.data.datasets[0].borderWidth = 1;
        } else {
            newState.data.datasets[0].pointHoverBorderWidth = 20;
            newState.data.datasets[0].pointRadius = 5;
            newState.data.datasets[0].pointHitRadius = 5;
            newState.data.datasets[0].borderWidth = 2;
        }
        this.setState(newState);
    }

    static getDerivedStateFromProps(props, state) {
        const newState = {...state};
        newState.data.labels = props.labels;
        newState.data.datasets[0].data = props.datasets;
        return newState;
    }

    render() {
        // Chart.defaults.global.defaultFontColor = "#fff";
        return (
            <div className="gl-chart">
                <WindowSizeListener onResize={this.onResize}/>
                <Line data={this.state.data} options={{
                    legend: {
                        display: false,
                    },
                    responsive: true,
                    layout: {
                        padding: {
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0
                        }
                    }
                }} />
            </div>
        )
    }
}

export default ChartComponent;