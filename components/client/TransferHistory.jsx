import React, { Component } from 'react';
import Link from 'next/link';
import moment from 'moment';
import http from '../../plugins/http';
// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
// Import components
import Heading from './Heading';
import Pagination from '../Pagination';
import * as historyActions from '../../redux/actions/history-actions';
import {formatNumber} from '../../helper/utils';

class TransferHistory extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.fetchHistories();
    }

    selectHistoryType = (type) => {
        return this.props.setHistoryTransferType(type);
    }

    loadData = (page) => {
        this.props.setHistoryCurrentPage(page);
    }

    fetchHistories = () => {
        if(this.props.wallet === 'CSE')
            this.props.fetchHistoryData();
        return this.props.setHistoryData([]);
    }

    renderHistory = (historiesData) => {
        let histories = [];
        for(let i in historiesData) {
            let history = historiesData[i];
            histories.push(
                <li key={ `history_${i}` } className="rs-border-top py-2 fs-table cusor-pointer" >
                    <a className="text-white" href={`https://csescan.io/transaction/${history.txId}`} target="_blank">
                        <div className="cse-history-table-row d-flex px-lg-3 py-1 align-items-center text-center justify-content-between">
                            <div className="col-1 text-left">
                                <img className="transaction-type" src={ history.type === 'SEND' ? "./images-asset/history-send.png" : "./images-asset/history-receive.png" }/>
                            </div>
                            <div className="col-2 text-left"> 
                                { moment(history.createdAt).format('DD-MM-YYYY HH:mm:ss') }
                            </div>
                            <div className="col-3 text-left">
                                <span className="d-lg-inline d-none">{ history.fromAddress }</span>
                            </div>
                            <div className="col-1 align-items-center flex-row-lg-reverse justify-content-between">
                                { history.coinAsset }
                            </div>
                            <div className="col-1 align-items-center flex-row-lg-reverse justify-content-between overflow-scroll-hidden">
                                { formatNumber(history.amount) }
                            </div>
                            <div className="col-3 align-items-center flex-row-lg-reverse justify-content-between overflow-scroll-hidden">
                                { history.txId }
                            </div>
                            <div className="col-1 text-right text-capitalize">
                                {
                                    history.status === 'PENDING' && (
                                        <span className="cl-pending span-status-custom">Pending</span>
                                    )
                                }
                                {
                                    history.status === 'SUCCESS' && (
                                        <span className="cl-success span-status-custom">Success</span>
                                    )
                                }
                                {
                                    history.status === 'CANCEL' && (
                                        <span className="cl-error span-status-custom">Cancel</span>
                                    )
                                }
                            </div>
                        </div>
                    </a>
                </li>
            )
        }
        if (histories.length) {
            return histories;
        }
        if (this.props.historyReducer.isFetching) {
            return ''
        } else {
            return (
                <div className="py-2 fs-table text-center text-white">
                    No data found!
                </div>
            )
        }
    }

    renderMobileHistory = (historiesData) => {
        let histories = [];
        for(let i in historiesData) {
            let history = historiesData[i];
            histories.push(<li key={ `history_${i}` } className="rs-border-top py-2 fs-table cusor-pointer">
                <a className="text-white" href={`https://csescan.io/transaction/${history.txId}`} target="_blank">
                    <div className="cse-mobile-history-table-row row">
                        <div className="col-2 pr-0 overflow-hidden">
                            <img className="transaction-type" src={ history.type === 'SEND' ? "./images-asset/history-send.png" : "./images-asset/history-receive.png" }/>
                        </div>
                        <div className="col-6 p-0 d-block overflow-hidden">
                            <div className="from-address d-flex">
                                <span>From </span>&nbsp;<span>{ history.fromAddress }</span>
                            </div>
                            <div className="transaction-time">
                                { moment(history.createdAt).format('DD.MM.YYYY') }
                            </div>
                        </div>
                        <div className="col-4 text-right overflow-hidden coin-and-amount d-flex" style={{ color: history.type === 'SEND' ? '#FBBF23' : '#50BFC3;'}}>
                            <div className="amount">{ history.type === 'SEND' ? '-' : '+'}&nbsp;{ formatNumber(history.amount) }</div>
                            <span className="coin-asset">&nbsp;{ history.coinAsset }</span>
                        </div>
                    </div>
                </a>    
            </li>)
        }
        if(histories.length){
            histories.push(<div key="devider" className="devider"/>);
            return histories;
        }
        if (this.props.historyReducer.isFetching) {
            return ''
        } else {
            return (
                <div className="py-2 fs-table text-center text-white">
                    No data found!
                </div>
            )
        }
    }

    render() {
        let historiesData = this.props.historyReducer.histories;
        return (
            <div className={`main-introduction ${window.innerWidth > 768 ? '' : 'pl-4 pr-4'}`}>
                <div className="py-4 d-flex justify-content-between">
                    {window.innerWidth > 768 && <Heading heading='Transfer History' />}
                    <div className="gl-cus-dropdown text-center text-capitalize d-lg-block d-none">
                        { this.props.historyReducer.transferFilterType == 'SEND' ? 'Sent' : this.props.historyReducer.transferFilterType == 'RECEIVE' ? 'Received' : 'All' }
                        <svg width="9" height="6" viewBox="0 0 9 6" fill="none" xmlns="http://www.w3.org/2000/svg" className="icon">
                            <path d="M5.01946 5.81124C4.89971 5.97715 4.65267 5.97715 4.53292 5.81124L0.738374 0.553353C0.595184 0.354943 0.736957 0.0777914 0.981639 0.0777914L8.57074 0.0777921C8.81543 0.0777921 8.9572 0.354945 8.81401 0.553354L5.01946 5.81124Z" fill="#34AEC3"/>
                        </svg>
                        <div className="gl-cus-dropdown__content">
                            <div className="p-3 items mt-3" onClick={() => this.selectHistoryType('')}>
                                All
                            </div>
                            <div className="p-3 items" onClick={() => this.selectHistoryType('SEND')}>
                                Sent
                            </div>
                            <div className="p-3 items mb-3" onClick={() => this.selectHistoryType('RECEIVE')}>
                                Received
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mb-4 d-flex gl-custom-border d-lg-none">
                    <Link href="#">
                        <div className={`col p-2 text-center  ${ this.props.historyReducer.transferFilterType === '' ? 'active' : ''}` } onClick={() => this.selectHistoryType('')}>
                            All
                        </div>
                    </Link>
                    <Link href="#">
                        <div className={`col p-2 text-center  ${ this.props.historyReducer.transferFilterType === 'SEND' ? 'active' : ''}` } onClick={() => this.selectHistoryType('SEND')}>
                            Sent
                        </div>
                    </Link>
                    <Link href="#">
                        <div className={`col p-2 text-center  ${ this.props.historyReducer.transferFilterType === 'RECEIVE' ? 'active' : ''}` }  onClick={() => this.selectHistoryType('RECEIVE')}>
                            Received
                        </div>
                    </Link>
                </div>
                <div className="bdr-10 rs-box-shadow gl-table-bg px-xl-2 position-relative">
                    <div className="d-lg-flex d-none px-3 py-4 align-items-center text-center justify-content-between">
                        <div className="col-1 text-left cse-table-column-title"> Action </div>
                        <div className="col-2 align-items-center flex-row-lg-reverse justify-content-between cse-table-column-title"> Time </div>
                        <div className="col-3 align-items-center flex-row-lg-reverse justify-content-between cse-table-column-title"> From </div>
                        <div className="col-1 align-items-center flex-row-lg-reverse justify-content-between cse-table-column-title"> Coin </div>
                        <div className="col-1 align-items-center flex-row-lg-reverse justify-content-between cse-table-column-title"> Amount </div>
                        <div className="col-3 align-items-center flex-row-lg-reverse justify-content-between cse-table-column-title"> Txid </div>
                        <div className="col-1 text-right cse-table-column-title"> Status </div>
                    </div>

                    <div className={`account__waiting-popup-content history-table-loading ${this.props.historyReducer.isFetching ? 'd-block' : 'd-none'}`}>
                        <i className='fas fa-spinner'></i>
                    </div>
                    <ul className={`p-0 m-0 list-table ${this.props.historyReducer.isFetching ? 'opacity-3': ''}`}>
                        { window.innerWidth > 768 ? this.renderHistory(historiesData) : this.renderMobileHistory(historiesData) }
                    </ul>
                    { !!historiesData.length && (<Pagination total={this.props.historyReducer.total} limit={this.props.historyReducer.limit} page={this.props.historyReducer.page} pages={this.props.historyReducer.pages} limitLoad={ limit => this.props.setHistoryCurrentPageLimit(limit) } loadData={ this.loadData } router={ this.props.router }/>) }
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    walletReducer: state.walletReducer,
    historyReducer: state.historyReducer,
});
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    historyActions,
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(TransferHistory);