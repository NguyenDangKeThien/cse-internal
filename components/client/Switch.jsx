import React, { Component, Fragment } from 'react';

export default class Switch extends Component {
    constructor(props) {
        super(props);
    
        const checked = props.checked;
    
        this.state = {
          checked,
        };
    }

    static getDerivedStateFromProps(props, state) {
        if ('checked' in props) {
          return {
            ...state,
            checked: props.checked,
          };
        }
        return null;
    }

    handleCheckboxChange = event => {
        this.props.onCheckboxChange(this.props.name, event.target.checked);
    }

    render() {
        return (
            <Fragment>
                <label className="switch-options">
                    <input type="checkbox" className="d-none" name="checked" 
                    onChange={this.handleCheckboxChange}
                    checked={this.state.checked}/>
                    <span className={`${this.state.checked ? 'custom-slider-on' : 'custom-slider-off'} slider round`}></span>
                </label>
            </Fragment>
        )
    }
}
